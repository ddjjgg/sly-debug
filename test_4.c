/*!****************************************************************************
 *
 * 	@file
 *	test_4.c
 *
 *	This file just tests if disabling SlyDebug works properly; meaning
 *	that every function macro turns into a no-op, but argument side-effects
 *	are preserved.
 *
 *****************************************************************************/

#include "stdio.h"
#include "stdlib.h"

#define SLY_DEBUG_ENABLE		(0)

#include "SlyDebug.h"


int main()
{
	//Take note of how many of these arguments have side-effects! These
	//side-effects should be preserved when SlyDebug is disabled.

	int number =	0;
	int * int_ptr =	NULL;

	
	SLY_DUP_DEBUG_FILE_PTR (stdout);

	SLY_SET_DEBUG_FILE (1, "test_4.txt");

	SLY_FLUSH_DEBUG_FILE();

	SLY_CLOSE_DEBUG_FILE();


	SLY_PRINT_DEBUG
	(
		"This message should not print out at all, now "
		"here's a fun number: %d",
		number++
	);

	SLY_PRINT_IF (number++, "This should not print out.");

	SLY_PRINT_IF_FP (stdout, number++, "This should not print out either.");

	SLY_NULL_CHECK (number++);

	SLY_NULL_CHECK_FP (stdout, number++);

	SLY_PRINT_DEBUG ("malloc() result is %p", int_ptr = malloc (sizeof (int)));


	//Check if the argument side effects were preserved

	printf ("#number = %d\n", number);

	if (5 != number)
	{
		printf ("FAIL: #number should have been 5\n");
	}


	printf ("#int_ptr = %p\n", int_ptr);

	if (NULL == int_ptr)
	{
		printf ("FAIL: #int_ptr should not have been NULL\n");
	}

	free (int_ptr);


	return 0;
}


