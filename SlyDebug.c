/*!*****************************************************************************
 *
 * @file
 * SlyDebug.c
 *
 * Provides the implementation of internal SlyDebug functions.
 *
 *
 * <b> KNOWN DESIGN QUIRKS </b>
 *
 * 1)
 *	The test-benches in this file are somewhat ugly, and require manual
 *	monitoring. The alternative is to write some non-trivial string
 *	interpretation functions to produce automatic test-benches instead,
 *	which may be done in a later version of this library. It should be
 *	noted that there exist test-benches in this project which are separate
 *	from this file (because they make use of different compile-time
 *	arguments).
 *
 * 2)
 *	Global variables are used to manage the output debug file. The
 *	motivation for doing this is that the calling program is absolved from
 *	having to pass around debug file handles from function to function,
 *	making it SIGNIFICANTLY easier to start using SlyDebug in a
 *	code-base at any point.
 *
 *	Additionally, none of these global variables make use of dynamic
 *	memory, as a way obviating the need for a "SLY_START ()" and
 *	"SLY_STOP ()" to allocate memory in the calling program. (That's not
 *	to say SlyDebug never calls malloc (), which it does during message
 *	generation).
 *
 *	As a result, though, SlyDebug can only remember information about 1
 *	debug file at a time (though the calling program can choose to switch
 *	said file at any time). Also, the internal management of said debug
 *	file information is admittedly somewhat awkward.
 *
 *	As a side-effect, while it is possible to directly print to a supplied
 *	#FILE pointer, it is NOT possible for SlyDebug to tell if another
 *	thread is printing to that #FILE at the same time. That is why ALL
 *	SlyDebug printing attempts make use of the same debug printing lock.
 *
 *	(The amount of time spent being locked should be short, however,
 *	because the debug string generation is done before the lock is
 *	acquired, and the lock is only needed as the generated string is dumped
 *	to the destination file).
 *
 * 3)
 *	The functions in this file take large argument lists. Why not just
 *	compact some of those arguments as structs, and reduce the number of
 *	arguments?
 *
 *	It is because, in general, these functions will not be called directly,
 *	but through macros instead. It is much cleaner on the macro's side of
 *	things to produce a long list of arguments rather than producing a
 *	struct and then calling the function.
 *
 *	Consider this: if you want to use the macro as an assignment rvalue,
 *	how would you generate a struct during that macro? (That is, if the
 *	macro should be a valid rvalue in the first place)
 *
 *	FIXME : This isn't true. The macro's already expand to do-while loops,
 *	in which local variables can be declared. Also take a look at GCC
 *	compound statements, which can help achieve the same thing with
 *	do-while loops.
 *
 * 4)
 *	The functions in this file have really long names. While not a major
 *	concern, it makes these functions more readable in backtraces whenever
 *	they appear.
 *
 * 5)
 *	The function internal_sly_va_print_debug_str () makes use of a "field
 *	string" argument which determines the order of different debug message
 *	fields.
 *
 *	This string could use a more complex scheme to contain more information
 *	about the debug message (thus reducing the total number of arguments
 *	to the function), but for the sake of simplicity (or laziness), this
 *	was decided against.
 *
 * 6)
 *	Take note that strings are generated in a buffer, and then usually
 *	dumped out in just one fprintf() call. The purpose behind this is that
 *	it was experimentally determined to be faster than using multiple
 *	fprintf() calls "along the way".
 *
 *	Additionally, by using just one fprintf() call, the amount of time the
 *	debug file lock has to be acquired is significantly reduced.
 *
 * 7)
 *	Take note that there is no internal_sly_erase_debug_file (). If
 *	internal_sly_dup_debug_file_ptr () is supported, then it is NOT always
 *	possible to delete the current debug file, because the saved FILE *
 *	might not refer to a "real" file (e.g. #stdout and #stderr). Getting a
 *	file name based on a FILE * is not always possible either, so that
 *	doesn't offer any help.
 *
 *	Of course, the file path could be saved when internal_sly_set_file ()
 *	is used instead, and only then would internal_sly_erase_debug_file ()
 *	delete a file...
 *
 *	For now, the suggested solution is using SLY_CLOSE_DEBUG_FILE () and
 *	then remove().
 *
 *****************************************************************************/




#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <errno.h>
#include <string.h>
#include <execinfo.h>
#include <pthread.h>
#include "SlyDebug.h"




/// The file stream that will be the target of printed debug messages

static FILE * sly_debug_file_fp		= NULL;


/// Indicates if the debug file needs to be closed by SlyDebug when
/// switching to a different file with internal_sly_set_debug_file() or
/// internal_sly_dup_debug_file_ptr()

int sly_debug_file_requires_close	= 0;


/// Indicates if the debug file pointer is NULL simply due to it being
/// uninitialized

int sly_debug_file_needs_init		= 1;


/// A lock indicating whether or not another internal SlyDebug function has
/// control over the debug file.

pthread_mutex_t sly_print_debug_lock = PTHREAD_MUTEX_INITIALIZER;




/// Obtains the lock associated with #sly_debug_file_fp. The function
/// internal_sly_print_debug_lock_release() MUST be called sometime after
/// calling this function, otherwise dead-lock WILL occur.
///
/// @param use_lock	Indicates whether or not this function should obtain
///			the lock. Why does this argument exist? It's prettier
///			for this function to handle that instead of several
///			if-else structures in other functions.
///
/// @return		Currently always 0

static int internal_sly_print_debug_lock_acquire (int use_lock)
{
	if (use_lock)
	{
		pthread_mutex_lock (&sly_print_debug_lock);
	}

	return 0;
}




/// Frees the lock associated with #sly_debug_file_fp. ONLY threads that have
/// previously called internal_sly_print_debug_lock_acquire() should call this
/// function, otherwise the lock can be obtained by two threads at once.
///
/// @param use_lock	Indicates whether or not this function should obtain
///			the lock. Why does this argument exist? It's prettier
///			for this function to handle that instead of several
///			if-else structures in other functions.
///
/// @return		Currently always 0

static int internal_sly_print_debug_lock_release (int use_lock)
{
	if (use_lock)
	{
		pthread_mutex_unlock (&sly_print_debug_lock);
	}

	return 0;
}




/// Used by internal_sly_get_errno_str () to easily produce a string
/// representing #errno
///
/// @{

#define EASY_ERRNO_STR_LEN		(255)


typedef struct
{
	char str [EASY_ERRNO_STR_LEN];

} EasyErrnoStr;

/// @}




/// Returns a human readable #errno string from an #errno value

EasyErrnoStr internal_sly_get_errno_str (int errnum)
{
	EasyErrnoStr es;

	strerror_r (errnum, es.str, EASY_ERRNO_STR_LEN);

	es.str [EASY_ERRNO_STR_LEN - 1] = '\0';

	return es;
}




/// Duplicates the file pointer #debug_file_ptr, and sets global variables
/// such that #sly_debug_file_fp will use that duplicate.
///
/// Why is dup() used in this function, instead of just setting
/// #sly_debug_file_fp to #debug_file_ptr directly? It is because if fclose()
/// is later called on #debug_file_ptr by external code, we still want
/// #sly_debug_file_fp to be a valid file pointer.
///
/// @param use_lock		Flag for whether this function should observe
///				the lock for #sly_debug_file_fp or not
///
/// @param complain_flag	Flag for whether errors (if any) should be
///				printed to #stderr
///
/// @param debug_file_ptr	Pointer to the #FILE that #sly_debug_file_fp
///				should point to
///
/// @param source_file_str	A string representing the source file of the
///				calling function
///
/// @param function_name_str	A string representing the name of the calling
///				function
///
/// @param line_number		An integer representing the line number this
///				function was called from
///
/// @return			0 upon success, -1 upon failure

int internal_sly_dup_debug_file_ptr
(
	int		use_lock,
	int		complain_flag,
	FILE *		debug_file_ptr,

	const char *	source_file_str,
	const char *	function_name_str,
	int		line_number
)
{
	//Sanity check on debug string arguments

	if (NULL == source_file_str) {source_file_str = "?";}
	if (NULL == function_name_str) {function_name_str = "?";}


	//Get the debug printing lock

	internal_sly_print_debug_lock_acquire (use_lock);


	//Check if debug file pointer should be set to #stderr due to a NULL
	//file pointer

	if (NULL == debug_file_ptr)
	{
		if (complain_flag)
		{
			fprintf
			(
				stderr,

				"ERROR : %s from %s, %s, %d : "
				"Instructed to set the debug "
				"file pointer to NULL, which will not be "
				"performed. The debug file pointer has been "
				"set to #stderr instead.\n",
				__func__,
				source_file_str,
				function_name_str,
				line_number
			);
		}


		//Attempt to close the current debug file in case it is
		//already being used

		internal_sly_close_debug_file
		(
			0,
			complain_flag,

			source_file_str,
			function_name_str,
			line_number
		);


		//Release the debug printing lock

		internal_sly_print_debug_lock_release (use_lock);

		return -1;
	}


	//Attempt to close the debug file in case it is already being used.

	internal_sly_close_debug_file
	(
		0,
		complain_flag,

		source_file_str,
		function_name_str,
		line_number
	);


	//Get the file number associated with the provided file pointer, and
	//check for errors.

	int file_num = fileno (debug_file_ptr);

	if (-1 == file_num)
	{
		if (complain_flag)
		{
			fprintf
			(
				stderr,

				"ERROR : %s from %s, %s, %d : "
				"Could not get fileno for "
				"the file ptr %p. Setting debug file pointer "
				"to #stderr as a fall-back.\n"
				"errno : %s\n",
				__func__,
				source_file_str,
				function_name_str,
				line_number,
				debug_file_ptr,
				internal_sly_get_errno_str (errno).str
			);
		}

		internal_sly_close_debug_file
		(
			0,
			complain_flag,

			source_file_str,
			function_name_str,
			line_number
		);

		internal_sly_print_debug_lock_release (use_lock);

		return -1;
	}


	//Make a duplicate of that fileno

	int dup_file_num = dup (file_num);

	if (-1 == dup_file_num)
	{
		if (complain_flag)
		{
			fprintf
			(
				stderr,

				"ERROR : %s from %s, %s, %d : "
				"Could not make duplicate fileno "
				"for fileno %d. Setting debug file pointer "
				"to #stderr as a fall-back.\n"
				"errno : %s\n",
				__func__,
				source_file_str,
				function_name_str,
				line_number,
				file_num,
				internal_sly_get_errno_str (errno).str
			);
		}

		internal_sly_close_debug_file
		(
			0,
			complain_flag,

			source_file_str,
			function_name_str,
			line_number
		);

		internal_sly_print_debug_lock_release (use_lock);

		return -1;
	}


	//Opens a file using the duplicated file pointer, so that the
	//generated file pointer refers to the same file as #debug_file_ptr.
	//
	//Doing this allows for external code to safely fclose() #debug_file_ptr
	//whilst #dup_file_ptr is still in use
	//
	//Note that the mode here is "a" and not "a+". This is needed to make
	//fdopen() succeed when #debug_file_ptr is an unreadable stream, like
	//#stdout or #stderr.

	FILE * dup_ptr = fdopen (dup_file_num, "a");

	if (NULL == dup_ptr)
	{
		if (complain_flag)
		{
			fprintf
			(
				stderr,

				"ERROR : %s from %s, %s, %d : "
				"Could not get file pointer for "
				"fileno %d. Setting debug file pointer "
				"to #stderr as a fall-back.\n"
				"errno : %s\n",
				__func__,
				source_file_str,
				function_name_str,
				line_number,
				dup_file_num,
				internal_sly_get_errno_str (errno).str
			);
		}

		internal_sly_close_debug_file
		(
			0,
			complain_flag,

			source_file_str,
			function_name_str,
			line_number
		);


		internal_sly_print_debug_lock_release (use_lock);

		return -1;
	}


	//Note, close () does NOT have to be called on #dup_file_num, because
	//fdopen () will ensure that the file-descriptor will be freed when
	//fclose () is used.


	//Set the debug file pointer
	//
	//(Note that #sly_debug_file_fp DOES require closing after this
	//function, because the FILE * that it gets set to is one that we
	//create)

	sly_debug_file_fp		= dup_ptr;
	sly_debug_file_requires_close	= 1;
	sly_debug_file_needs_init	= 0;


	//Release the debug printing lock

	internal_sly_print_debug_lock_release (use_lock);

	return 0;
}




/// Opens a file and sets #sly_debug_file_fp to point to that #FILE
///
/// @param use_lock		Flag for whether this function should observe
///				the lock for #sly_debug_file_fp or not
///
/// @param complain_flag	Flag for whether errors (if any) should be
///				printed to #stderr
///
/// @param truncate_flag	Flag for whether the opened file should be
///				erased at opening, or if printing attempts to
///				it should just append to the end of it.
///
/// @param debug_file_path_str	A string indicating what file should be
///				opened/created
///
/// @param source_file_str	A string representing the source file of the
///				calling function
///
/// @param function_name_str	A string representing the name of the calling
///				function
///
/// @param line_number		An integer representing the line number this
///				function was called from
///
/// @return			0 upon success, -1 upon failure

int internal_sly_set_debug_file
(
	int		use_lock,
	int		complain_flag,
	int		truncate_flag,
	const char *	debug_file_path_str,

	const char *	source_file_str,
	const char *	function_name_str,
	int		line_number
)
{
	//Sanity check on debug string arguments

	if (NULL == source_file_str) {source_file_str = "?";}
	if (NULL == function_name_str) {function_name_str = "?";}


	//Get the debug printing lock

	internal_sly_print_debug_lock_acquire (use_lock);


	//Check if debug file pointer should be set to #stderr because the file
	//path is NULL

	if (NULL == debug_file_path_str)
	{

		if (complain_flag)
		{
			fprintf
			(
				stderr,

				"ERROR : %s from %s, %s, %d : "
				"Argument #debug_file_path_str "
				"pointed to NULL; setting file to "
				"#stderr instead.\n",
				__func__,
				source_file_str,
				function_name_str,
				line_number
			);
		}


		//Set the debug file to #stderr

		internal_sly_close_debug_file
		(
			0,
			complain_flag,

			source_file_str,
			function_name_str,
			line_number
		);


		//Release the debug printing lock

		internal_sly_print_debug_lock_release (use_lock);

		return -1;
	}


	//Check if debug file pointer should be set to #stderr due to a file
	//path with only a NULL-terminator

	if ('\0' == (debug_file_path_str [0]))
	{
		if (complain_flag)
		{
			fprintf
			(
				stderr,

				"ERROR : %s from %s, %s, %d : "
				"Argument #debug_file_path_str "
				"was a string of 0 length; setting file to "
				"#stderr instead.\n",
				__func__,
				source_file_str,
				function_name_str,
				line_number
			);
		}


		//Set the debug file to #stderr

		internal_sly_close_debug_file
		(
			0,
			complain_flag,

			source_file_str,
			function_name_str,
			line_number
		);


		//Release the debug printing lock

		internal_sly_print_debug_lock_release (use_lock);

		return -1;
	}


	//Attempt to close the debug file in case it is already being used.

	internal_sly_close_debug_file
	(
		0,
		complain_flag,

		source_file_str,
		function_name_str,
		line_number
	);


	//Open the new file

	FILE * file_ptr = NULL;

	if (truncate_flag)
	{
		file_ptr = fopen (debug_file_path_str, "w+");
	}

	else
	{
		file_ptr = fopen (debug_file_path_str, "a+");
	}


	//Check for any errors that occurred in opening the file

	if (NULL == file_ptr)
	{
		fprintf
		(
			stderr,

			"ERROR : %s from %s, %s, %d : "
			"Could not open file \"%s\" "
			"to save debug messages. Debug messages will be "
			"directed to #stderr instead.\n",
			__func__,
			source_file_str,
			function_name_str,
			line_number,
			debug_file_path_str
		);


		//Set the debug file to #stderr

		internal_sly_close_debug_file
		(
			0,
			complain_flag,

			source_file_str,
			function_name_str,
			line_number
		);


		//Release the debug printing lock

		internal_sly_print_debug_lock_release (use_lock);

		return -1;
	}


	//Set the debug file to the opened file
	//
	//Make sure to indicate that the file should be closed, and that the
	//file has been initialized

	sly_debug_file_fp		= file_ptr;
	sly_debug_file_requires_close	= 1;
	sly_debug_file_needs_init	= 0;


	//Release the debug printing lock

	internal_sly_print_debug_lock_release (use_lock);

	return 0;
}




/// Flushes all pending writes to #sly_debug_file_fp, which in practice means
/// those messages get saved.
///
/// @param use_lock		Flag for whether this function should observe
///				the lock for #sly_debug_file_fp or not
///
/// @param complain_flag	Flag for whether errors (if any) should be
///				printed to #stderr
///
/// @param source_file_str	A string representing the source file of the
///				calling function
///
/// @param function_name_str	A string representing the name of the calling
///				function
///
/// @param line_number		An integer representing the line number this
///				function was called from
///
/// @return			0 upon success, -1 upon failure

int internal_sly_flush_debug_file
(
	int		use_lock,
	int		complain_flag,

	const char *	source_file_str,
	const char *	function_name_str,
	int		line_number
)
{
	//Sanity check on debug string arguments

	if (NULL == source_file_str) {source_file_str = "?";}
	if (NULL == function_name_str) {function_name_str = "?";}


	//Get the debug printing lock

	internal_sly_print_debug_lock_acquire (use_lock);


	//Check if there is a file to flush

	if (NULL == sly_debug_file_fp)
	{
		if (complain_flag)
		{
			fprintf
			(
				stderr,

				"ERROR : %s from %s, %s, %d : "
				"Encountered a NULL file pointer. "
				"Setting debug file to #stderr as "
				"a fallback.\n",
				__func__,
				source_file_str,
				function_name_str,
				line_number
			);
		}


		//Set the debug file to #stderr

		internal_sly_close_debug_file
		(
			0,
			complain_flag,

			source_file_str,
			function_name_str,
			line_number
		);


		//Release the debug printing lock

		internal_sly_print_debug_lock_release (use_lock);

		return -1;
	}


	//Flush the file

	fflush (sly_debug_file_fp);


	//Release the debug printing lock

	internal_sly_print_debug_lock_release (use_lock);

	return 0;
}




/// Closes #sly_debug_file_fp and sets it to #stderr, assuring that all writes
/// to it get saved, and the system resources associated with that file get
/// freed.
///
/// Notably, this function will only close #sly_debug_file_fp if
/// #sly_debug_file_requires_close is true. This is done to avoid closing
/// #stderr, which #sly_debug_file_fp is set to after using this function.
///
/// @param use_lock		Flag for whether this function should observe
///				the lock for #sly_debug_file_fp or not
///
/// @param complain_flag	Flag for whether errors (if any) should be
///				printed to #stderr. (Unused currently, but
///				included to anticipate likely interface change
///				for it usage)
///
/// @param complain_flag	Flag for whether errors (if any) should be
///				printed to #stderr
///
/// @param source_file_str	A string representing the source file of the
///				calling function (unused currently)
///
/// @param function_name_str	A string representing the name of the calling
///				function (unused currently)
///
/// @param line_number		An integer representing the line number this
///				function was called from (unused currently)
///
/// @return			0 upon success, -1 upon failure

int internal_sly_close_debug_file
(
	int		use_lock,
	int		complain_flag,

	const char *	source_file_str,
	const char *	function_name_str,
	int		line_number

)
{
	// This code is disabled, because there are no error messages in this
	// function currently.
	//
	// //Sanity check on debug string arguments
	//
	// if (NULL == source_file_str) {source_file_str = "?";}
	// if (NULL == function_name_str) {function_name_str = "?";}


	//Get the debug printing lock

	internal_sly_print_debug_lock_acquire (use_lock);


	//Close the file, assuming there is one to close, and it is one that we
	//should be closing in the first place.

	if ((NULL != sly_debug_file_fp) && (0 != sly_debug_file_requires_close))
	{
		fclose (sly_debug_file_fp);
	}


	//Make sure that any later debug print attempts can still go to #stderr
	//at least

	sly_debug_file_fp		= stderr;
	sly_debug_file_requires_close	= 0;
	sly_debug_file_needs_init	= 0;


	//Release the debug printing lock

	internal_sly_print_debug_lock_release (use_lock);

	return 0;
}




/// Prints out a string to a destination string in the same fashion as
/// vsnprintf(), but more info is generated to make it easier to continuously
/// print strings to sections of one larger string.
///
/// @param dst_str		The string to print the new string to
///
/// @param dst_str_len		The number of characters that can be printed to
///				#dst_str, INCLUDING the null-terminator
///
/// @param rem_str_ptr		Where to place a pointer to the null-terminator
///				in the printed string. This argument is useful
///				as the #dst_str argument to a future call to
///				rem_snprintf(), to concatenate another string
///				after what was just printed.
///
/// @param rem_str_len_ptr	Where to place the length of the "remaining
///				string". This is useful as the #dst_str_len
///				argument to a future call to rem_snprintf(),
///				to concatenate another string after what was
///				just printed
///
/// @param print_len_ptr	Where to place the length of the string that
///				was printed, NOT including the null-terminator
///
/// @param tally_chars_ptr	A pointer to a value to ADD the number of
///				printed characters, NOT including the
///				null-terminator. It is important to note that a
///				value is added to whatever was originally at
///				#tally_chars_ptr.
///
/// @param str_truncated	Where to place a flag, which evaluates to true
///				if characters had to be truncated from the
///				printed string due to lack of space
///
/// @param format_str		The format string that defines how the string
///				should be printed, just as in snprintf()
///
/// @param argument_list	The values referenced in the format string,
///				just like the #va_list argument in vsnprintf()
///
/// @return			-1 upon failure, 0 upon success

static int rem_vsnprintf
(
	char *		dst_str,
	size_t		dst_str_len,
	char **		rem_str_ptr,
	size_t *	rem_str_len_ptr,
	size_t *	print_len_ptr,
	size_t *	tally_chars_ptr,
	int *		str_truncated_ptr,
	const char *	format_str,
	va_list		argument_list
)
{
	//If #dst_str is NULL, make it so that vsnprintf() will still properly
	//work.
	//
	//Why not just leave the function early? Simply so that #print_len_ptr
	//can still be calculated, which is still useful information even if no
	//string is actually printed.

	char * internal_dst_str =	"";
	size_t internal_dst_str_len =	0;

	if (NULL != dst_str)
	{
		internal_dst_str =	dst_str;
		internal_dst_str_len =	dst_str_len;
	}


	//Print the desired string to #dst_str

	int print_len = 0;

	if (NULL != format_str)
	{
		print_len =	vsnprintf
				(
					internal_dst_str,
					internal_dst_str_len,
					format_str,
					argument_list
				);
	}


	//Normalize #print_len so that it includes the printed null-terminator

	print_len = print_len + 1;


	//Determine where the "remaining string" would start, and what length
	//it could have.
	//
	//Keep in mind, if #dst_str was NULL, then no string was actually
	//printed, so #rem_str_ptr must reflect this.

	if (NULL == dst_str)
	{
		if (NULL != rem_str_ptr)
		{
			*rem_str_ptr = NULL;
		}

		if (NULL != rem_str_len_ptr)
		{
			*rem_str_len_ptr = 0;
		}
	}

	else if (dst_str_len > print_len)
	{
		if (NULL != rem_str_ptr)
		{
			*rem_str_ptr =	(print_len > 0) ?
					(dst_str + print_len) - 1 :
					dst_str;
		}

		if (NULL != rem_str_len_ptr)
		{
			*rem_str_len_ptr =	(print_len > 0) ?
						dst_str_len - (print_len - 1) :
						dst_str_len;
		}
	}

	else
	{
		if (NULL != rem_str_ptr)
		{
			*rem_str_ptr = (dst_str + dst_str_len) - 1;
		}

		if (NULL != rem_str_len_ptr)
		{
			*rem_str_len_ptr = 0;
		}
	}


	//Represent how many char's should have been available (NOT including
	//the null-terminator) to fully print the string

	if (NULL != print_len_ptr)
	{
		*print_len_ptr =	(print_len > 0) ?
					(print_len - 1) :
					0;
	}


	//Add to the tally how many characters are needed in total (NOT
	//including the null-terminator) to fully print the string

	if (NULL != tally_chars_ptr)
	{
		*tally_chars_ptr =	(print_len > 0) ?
					*tally_chars_ptr + (print_len - 1) :
					*tally_chars_ptr;
	}


	//Signify if the string was truncated for any reason

	if (NULL != str_truncated_ptr)
	{
		*str_truncated_ptr =	(print_len > 0) ?
					(print_len > dst_str_len) :
					1;
	}


	//Return whether or not vsnprintf() attempt succeeded or not

	return (print_len > 1) ? 0 : -1;
}




/// Version of rem_vsnprintf() that uses '...' for the variadic arguments.

static int rem_snprintf
(
	char *		dst_str,
	size_t		dst_str_len,
	char **		rem_str_ptr,
	size_t *	rem_str_len_ptr,
	size_t *	print_len_ptr,
	size_t *	tally_chars_ptr,
	int *		str_truncated_ptr,
	const char *	format_str,
	...
)
{
	va_list argument_list;

	va_start (argument_list, format_str);

	int ret_val =	rem_vsnprintf
			(
				dst_str,
				dst_str_len,
				rem_str_ptr,
				rem_str_len_ptr,
				print_len_ptr,
				tally_chars_ptr,
				str_truncated_ptr,
				format_str,
				argument_list
			);

	va_end (argument_list);


	return ret_val;
}




/// Prints the SlyDebug "Time" field to a string
///
/// @param dst_str,		See rem_snprintf()
///
/// @param dst_str_len		See rem_snprintf()
///
/// @param rem_str_ptr		See rem_snprintf()
///
/// @param rem_str_len_ptr	See rem_snprintf()
///
/// @param print_len_ptr	See rem_snprintf()
///
/// @param tally_chars_ptr	See rem_snprintf()
///
/// @param str_truncated_ptr	See rem_snprintf()
///
/// @param time_pre_str		The string to print before the "Time" field.
///
/// @param time_post_str	The string to print after the "Time" field.
///
/// @return			See rem_snprintf()

static int internal_sly_print_time
(
	char *		dst_str,
	size_t		dst_str_len,
	char **		rem_str_ptr,
	size_t *	rem_str_len_ptr,
	size_t *	print_len_ptr,
	size_t *	tally_chars_ptr,
	int *		str_truncated_ptr,
	const char *	time_pre_str,
	const char *	time_post_str
)
{
	//String argument sanity checks

	if (NULL == time_pre_str)	{time_pre_str = "";}
	if (NULL == time_post_str)	{time_post_str = "";}


	//Fetch the current time

	const int CHARS_IN_TIME_STR = 26 + 1;	//Max characters needed by
						//asctime_r ()

	const int NEWLINE_INDEX = 24;		//Location of newlinew in string
						//produced by asctime_r ()


	time_t		raw_time;
	struct tm	time_info;
	char		time_str [CHARS_IN_TIME_STR];


	time (&raw_time);

	localtime_r (&raw_time, &time_info);

	asctime_r (&time_info, &time_str [0]);


	time_str [NEWLINE_INDEX] = '\0';


	//Print out the measured time

	return rem_snprintf
	(
		dst_str,
		dst_str_len,
		rem_str_ptr,
		rem_str_len_ptr,
		print_len_ptr,
		tally_chars_ptr,
		str_truncated_ptr,
		"%s%24s%s",
		time_pre_str,
		&time_str [0],
		time_post_str
	);
}




/// Prints the SlyDebug "Source" field to a string
///
/// @param dst_str,		See rem_snprintf()
///
/// @param dst_str_len		See rem_snprintf()
///
/// @param rem_str_ptr		See rem_snprintf()
///
/// @param rem_str_len_ptr	See rem_snprintf()
///
/// @param print_len_ptr	See rem_snprintf()
///
/// @param tally_chars_ptr	See rem_snprintf()
///
/// @param str_truncated_ptr	See rem_snprintf()
///
/// @param source_file_pre_str	The string to print before the "Source" field.
///
/// @param source_file_str	The name of the source file the debug message
///				originated from
///
/// @param source_file_post_str	The string to print after the "Source" field.
///
/// @return			See rem_snprintf()

static int internal_sly_print_source_file
(
	char *		dst_str,
	size_t		dst_str_len,
	char **		rem_str_ptr,
	size_t *	rem_str_len_ptr,
	size_t *	print_len_ptr,
	size_t *	tally_chars_ptr,
	int *		str_truncated_ptr,
	const char *	source_file_pre_str,
	const char *	source_file_str,
	const char *	source_file_post_str
)
{
	//String argument sanity checks

	if (NULL == source_file_pre_str)	{source_file_pre_str = "";}
	if (NULL == source_file_str)		{source_file_str = "";}
	if (NULL == source_file_post_str)	{source_file_post_str = "";}


	//Print out the source file string

	return rem_snprintf
	(
		dst_str,
		dst_str_len,
		rem_str_ptr,
		rem_str_len_ptr,
		print_len_ptr,
		tally_chars_ptr,
		str_truncated_ptr,
		"%s%s%s",
		source_file_pre_str,
		source_file_str,
		source_file_post_str
	);
}




/// Prints the SlyDebug "Function" field to a string
///
/// @param dst_str,		See rem_snprintf()
///
/// @param dst_str_len		See rem_snprintf()
///
/// @param rem_str_ptr		See rem_snprintf()
///
/// @param rem_str_len_ptr	See rem_snprintf()
///
/// @param print_len_ptr	See rem_snprintf()
///
/// @param tally_chars_ptr	See rem_snprintf()
///
/// @param str_truncated_ptr	See rem_snprintf()
///
/// @param function_pre_str	The string to print before the "Function" field.
///
/// @param function_str		The name of the function the debug message is
///				in reference to
///
/// @param function_post_str	The string to print after the "Function" field.
///
/// @return			See rem_snprintf()

static int internal_sly_print_function_name
(
	char *		dst_str,
	size_t		dst_str_len,
	char **		rem_str_ptr,
	size_t *	rem_str_len_ptr,
	size_t *	print_len_ptr,
	size_t *	tally_chars_ptr,
	int *		str_truncated_ptr,
	const char *	function_pre_str,
	const char *	function_str,
	const char *	function_post_str
)
{
	//String argument sanity checks

	if (NULL == function_pre_str)	{function_pre_str = "";}
	if (NULL == function_str)	{function_str = "";}
	if (NULL == function_post_str)	{function_post_str = "";}


	//Print out the function name string

	return rem_snprintf
	(
		dst_str,
		dst_str_len,
		rem_str_ptr,
		rem_str_len_ptr,
		print_len_ptr,
		tally_chars_ptr,
		str_truncated_ptr,
		"%s%s%s",
		function_pre_str,
		function_str,
		function_post_str
	);
}




/// Prints the SlyDebug "Line Number" field to a string
///
/// @param dst_str,		See rem_snprintf()
///
/// @param dst_str_len		See rem_snprintf()
///
/// @param rem_str_ptr		See rem_snprintf()
///
/// @param rem_str_len_ptr	See rem_snprintf()
///
/// @param print_len_ptr	See rem_snprintf()
///
/// @param tally_chars_ptr	See rem_snprintf()
///
/// @param str_truncated_ptr	See rem_snprintf()
///
/// @param line_num_pre_str	The string to print before the "Line Number" field.
///
/// @param line_num		The line number at which the debug message was
///				printed from
///
/// @param line_num_post_str	The string to print after the "Line Number" field.
///
/// @return			See rem_snprintf()

static int internal_sly_print_line_number
(
	char *		dst_str,
	size_t		dst_str_len,
	char **		rem_str_ptr,
	size_t *	rem_str_len_ptr,
	size_t *	print_len_ptr,
	size_t *	tally_chars_ptr,
	int *		str_truncated_ptr,
	const char *	line_num_pre_str,
	const int	line_num,
	const char *	line_num_post_str
)
{
	//String argument sanity checks

	if (NULL == line_num_pre_str)	{line_num_pre_str = "";}
	if (NULL == line_num_post_str)	{line_num_post_str = "";}


	//Print out the line number string

	return rem_snprintf
	(
		dst_str,
		dst_str_len,
		rem_str_ptr,
		rem_str_len_ptr,
		print_len_ptr,
		tally_chars_ptr,
		str_truncated_ptr,
		"%s%d%s",
		line_num_pre_str,
		line_num,
		line_num_post_str
	);
}




/// Prints out the current state of the function call stack.
///
/// Notably, this function gives much better results when the program is
/// compiled with the "-rdynamic" flag with "gcc". Otherwise, the stack trace
/// might not contain the actual function names.
///
/// @param dst_str,		See rem_snprintf()
///
/// @param dst_str_len		See rem_snprintf()
///
/// @param rem_str_ptr		See rem_snprintf()
///
/// @param rem_str_len_ptr	See rem_snprintf()
///
/// @param print_len_ptr	See rem_snprintf()
///
/// @param tally_chars_ptr	See rem_snprintf()
///
/// @param str_truncated_ptr	See rem_snprintf()
///
/// @param complain_flag	A flag indicating whether this function should
///				print error messages on #stderr when it fails
///				to execute
///
/// @param bt_numbered_flag	A flag indicating whether or not the backtrace
///				entries should be numbered or not
///
/// @param bt_max_depth		The maximum number of functions that can be
///				print out in the stack trace. The higher this
///				number, the more memory this function can
///				_potentially_ use. It is rare for programs to
///				reach a function call depth of more than 50, so
///				a value of 1024 should be good enough for 99.9%
///				of programs. (This is an #int because
///				backtrace() expects the length as an #int)
///
/// @param bt_trim_low		The number of functions to trim off the lowest
///				part of the stack trace (i.e. least recent
///				functions)
///
/// @param bt_trim_high		The number of functions to trim off the highest
///				part of the stack trace (i.e. most recent
///				functions)
///
/// @param bt_pre_str		A string to print out before the backtrace.
///				This must NOT be NULL.
///
/// @param bt_post_str		A string to print out after the backtrace.
///				This must NOT be NULL.
///
/// @param bt_entry_pre_str	A string to print out before every backtrace
///				entry. This must NOT be NULL.
///
/// @param bt_entry_post_str	A string to print out after every backtrace
///				entry. This must NOT be NULL.
///
/// @param bt_num_pre_str	A string to print out before every number
///				listing in the backtrace. This must NOT be
///				NULL.
///
/// @param bt_num_post_str	A string to print out after every number
///				listing in the backtrace. This must NOT be
///				NULL.
///
/// @return			0 upon success
///
///				-1 upon a generic failure
///
///				-2 for a failure due to (bt_max_depth <= 0)

static int internal_sly_print_backtrace
(
	char *		dst_str,
	size_t		dst_str_len,
	char **		rem_str_ptr,
	size_t *	rem_str_len_ptr,
	size_t *	print_len_ptr,
	size_t *	tally_chars_ptr,
	int *		str_truncated_ptr,

	int		complain_flag,
	int		bt_numbered_flag,

	int		bt_max_depth,
	unsigned int	bt_trim_low,
	unsigned int	bt_trim_high,


	const char *	bt_pre_str,
	const char *	bt_post_str,
	const char *	bt_entry_pre_str,
	const char *	bt_entry_post_str,
	const char *	bt_num_pre_str,
	const char *	bt_num_post_str
)
{
	//String arguments sanity check

	if (NULL == bt_pre_str)		{bt_pre_str = "";}
	if (NULL == bt_post_str)	{bt_post_str = "";}
	if (NULL == bt_entry_pre_str)	{bt_entry_pre_str = "";}
	if (NULL == bt_entry_post_str)	{bt_entry_post_str = "";}
	if (NULL == bt_num_pre_str)	{bt_num_pre_str = "";}
	if (NULL == bt_num_post_str)	{bt_num_post_str = "";}


	//Create variables to keep track of where items should be printed in
	//the destination string

	char * cur_str =	dst_str;
	size_t cur_str_len =	dst_str_len;
	size_t bt_char_tally =	0;


	//Check if backtrace should exit early, due to zero max depth

	if (bt_max_depth <= 0)
	{
		rem_snprintf
		(
			cur_str,
			cur_str_len,
			&cur_str,
			&cur_str_len,
			NULL,
			&bt_char_tally,
			str_truncated_ptr,
			"%s%sEMPTY DUE TO bt_max_depth = %0d%s%s",
			bt_pre_str,
			bt_entry_pre_str,
			bt_max_depth,
			bt_entry_post_str,
			bt_post_str
		);

		return -2;
	}


	//Print out the backtrace pre-text

	rem_snprintf
	(
		cur_str,
		cur_str_len,
		&cur_str,
		&cur_str_len,
		NULL,
		&bt_char_tally,
		str_truncated_ptr,
		"%s",
		bt_pre_str
	);


	// Obtain the current backtrace.
	//
	// Why is a do-while loop used for this process? backtrace() does not
	// expose what the total stack depth is, so when #bt_buf is not large
	// enough to hold the backtrace, we must "guess" a large enough length
	// to hold the complete backtrace.
	//
	// Why not just always use #bt_max_depth as the buffer
	// length? That's a memory inefficient solution (ex: what if
	// bt_max_depth is 4GB? Then every time this function is called,
	// 4GB will be used immediately.)

	const unsigned int BT_BUF_LEN_INC =	256;

	unsigned int bt_buf_len =		(bt_max_depth < BT_BUF_LEN_INC) ?
						(bt_max_depth) :
						BT_BUF_LEN_INC;

	void ** bt_buf =			NULL;

	int used_bt_buf_len =			0;

	do
	{
		//Attempt to allocate a buffer large enough to hold the
		//backtrace

		bt_buf = malloc (sizeof (void **) * bt_buf_len);

		if (NULL == bt_buf)
		{
			fprintf
			(
				stderr,
				"ERROR : %s : memory allocation "
				"of size %u bytes failed for #bt_buf\n",
				__func__,
				bt_buf_len
			);

			return -1;
		}


		//Get the backtrace

		used_bt_buf_len = backtrace (bt_buf, bt_buf_len);


		//Determine if the complete backtrace was captured

		if (used_bt_buf_len < bt_buf_len)
		{
			break;
		}


		//If the current backtrace depth has reached the max
		//depth, then accept the current backtrace as good
		//enough

		if (bt_max_depth <= bt_buf_len)
		{
			break;
		}


		//If the current backtrace depth couldn't capture the
		//entire backtrace, then make sure the next attempt
		//uses a larger backtrace buffer

		if (used_bt_buf_len >= bt_buf_len)
		{
			bt_buf_len = bt_buf_len + BT_BUF_LEN_INC;

			if (bt_buf_len > bt_max_depth)
			{
				bt_buf_len = bt_max_depth;
			}
		}


		//Free the memory used for this current backtrace
		//attempt

		free (bt_buf);

	} while (1);


	//Get the actual names of the functions to print out

	char ** bt_strings = backtrace_symbols (bt_buf, used_bt_buf_len);


	//Calculate where to start and end the printing of the backtrace
	//
	//(Necessary to do to avoid overflow errors)

	int print_start =	(bt_trim_high < used_bt_buf_len) ?
				(bt_trim_high) :
				(used_bt_buf_len);

	int print_stop =	(bt_trim_low < used_bt_buf_len) ?
				(used_bt_buf_len - bt_trim_low) :
				(0);


	//Print out the parts of the backtrace selected by the depth arguments

	for (int i = print_start; i < print_stop; i += 1)
	{
		if (bt_numbered_flag)
		{

			rem_snprintf
			(
				cur_str,
				cur_str_len,
				&cur_str,
				&cur_str_len,
				NULL,
				&bt_char_tally,
				str_truncated_ptr,
				"%s%0d%s%s%s%s",
				bt_num_pre_str,
				(i - print_start),
				bt_num_post_str,
				bt_entry_pre_str,
				bt_strings [i],
				bt_entry_post_str
			);
		}

		else
		{
			rem_snprintf
			(
				cur_str,
				cur_str_len,
				&cur_str,
				&cur_str_len,
				NULL,
				&bt_char_tally,
				str_truncated_ptr,
				"%s%s%s",
				bt_entry_pre_str,
				bt_strings [i],
				bt_entry_post_str
			);
		}
	}


	//Print out the backtrace post-text

	rem_snprintf
	(
		cur_str,
		cur_str_len,
		&cur_str,
		&cur_str_len,
		NULL,
		&bt_char_tally,
		str_truncated_ptr,
		"%s",
		bt_post_str
	);


	//Make sure to update #rem_str, #rem_str_len, #print_len_ptr, and
	//#tally_chars_ptr, because they were not passed to rem_snprintf()
	//directly

	if (NULL != rem_str_ptr)
	{
		*rem_str_ptr = cur_str;
	}

	if (NULL != rem_str_len_ptr)
	{
		*rem_str_len_ptr = cur_str_len;
	}

	if (NULL != print_len_ptr)
	{
		*print_len_ptr = bt_char_tally;
	}

	if (NULL != tally_chars_ptr)
	{
		*tally_chars_ptr = (*tally_chars_ptr) + bt_char_tally;
	}


	//Free the memory used to capture the backtrace

	free (bt_strings);

	free (bt_buf);


	return 0;
}




/// Gathers the information needed for a SlyDebug debug message, and prints
/// out a formatted debug message to a destination string.
///
/// @param dst_str,			See rem_snprintf()
///
/// @param dst_str_len			See rem_snprintf()
///
/// @param rem_str_ptr			See rem_snprintf()
///
/// @param rem_str_len_ptr		See rem_snprintf()
///
/// @param print_len_ptr		See rem_snprintf()
///
/// @param tally_chars_ptr		See rem_snprintf()
///
/// @param str_truncated_ptr		See rem_snprintf()
///
/// @param complain_flag		Flag for whether errors (if any) should
///					be printed to #stderr
///
/// @param backtrace_numbered_flag	Flag for whether backtrace entries
///					should be numbered or not
///
/// @param backtrace_max_depth		The maximum depth of the backtrace in
///					terms of number of entries
///
/// @param bt_trim_low			The number of functions to trim off the
///					lowest part of the stack trace (i.e.
///					least recent functions)
///
/// @param bt_trim_high			The number of functions to trim off the
///					highest part of the stack trace (i.e.
///					most recent functions)
///
/// @param source_file_pre_str		A string to print out before the source
///					file field
///
/// @param source_file_str		The source file name field string
///
/// @param source_file_post_str		A string to print out after the source
///					file field
///
/// @param function_name_pre_str	A string to print out before the
///					function name field
///
/// @param function_name_str		The function name field string
///
/// @param function_name_post_str	A string to print out after the
///					function name field
///
/// @param line_number_pre_str		A string to print out before the line
///					number field
///
/// @param line_number			The line number of where this function
///					was called from
///
/// @param line_number_post_str		A string to print out after the line
///					number field
///
/// @param time_pre_str			A string to print out before the time
///					field
///
/// @param time_post_str		A string to print out after the time
///					field
///
/// @param backtrace_pre_str		A string to print out before the stack
///					trace
///
/// @param backtrace_post_str		A string to print out after the stack
///					trace
///
/// @param backtrace_entry_pre_str	A string to print out before each stack
///					trace entry
///
/// @param backtrace_entry_post_str	A string to print out after each stack
///					trace entry
///
/// @param backtrace_num_pre_str	A string to print out before each stack
///					trace entry number
///
/// @param backtrace_num_post_str	A string to print out after each stack
///					trace entry number
///
/// @param outer_message_pre_str	A string to out before the entire debug
///					mesage
///
/// @param outer_message_post_str	A string to out after the entire debug
///					message
///
/// @param inner_message_pre_str	A string to print out before the
///					information field in the debug message
///
/// @param inner_message_post_str	A string to print out after the
///					information field in the debug message
///
/// @param field_str			A string indicating what fields will be
///					included in the debug message, and in
///					what order. This string should be NULL
///					terminated.
///
///					The following patterns in this
///					string have the following semantic
///					meaning:
///
///					b -> Backtrace Field
///
///					f -> Function Name Field
///
///					l -> Line Number Field
///
///					m -> Message Field
///
///					s -> Source File Field
///
///					t -> Time Field
///
///					So, for example, the string "sfltmb"
///					indicates the printing order
///					of {source file, function name, line
///					number, time, message, backtrace}.
///
///					All unrecognized patterns get ignored.
///
///					(Note how side-channel information
///					regarding field-options are
///					not included in this string even though
///					they technically could be. The reason
///					for this is that even though having
///					these as separate function arguments
///					adds a constant cost to the function,
///					the #field_str would quickly become
///					annoying if it contained options for
///					fields like the backtrace. Plus, the
///					field string parsing itself would not
///					come for free, because such
///					field-options would have to be detected
///					and interpreted. Having the field
///					options as mandatory function arguments
///					makes it significantly easier to
///					swap-out said options without string
///					manipulation, makes the #field_str
///					shorter, and makes it so that #field_str
///					does not need a variadic argument field
///					associated with it (like the
///					#format_str does)).
///
///					This argument may or may not become
///					irrelevant as debug field options are
///					added/removed in future versions)
///
/// @param format_str			A format string describing the actual
///					"message" of the debug message. This
///					format string follows the same
///					conventions as printf()
///
/// @param argument_list		The list of arguments referenced by
///					#format_str
///
/// @return				0 upon success, -1 upon failure

int internal_sly_va_print_debug_str
(
	char *		dst_str,
	size_t		dst_str_len,
	char **		rem_str_ptr,
	size_t *	rem_str_len_ptr,
	size_t *	print_len_ptr,
	size_t *	tally_chars_ptr,
	int *		str_truncated_ptr,

	int		complain_flag,

	int		backtrace_numbered_flag,

	int		backtrace_max_depth,
	unsigned int	backtrace_trim_depth_low,
	unsigned int	backtrace_trim_depth_high,

	const char *	source_file_pre_str,
	const char *	source_file_str,
	const char *	source_file_post_str,
	const char *	function_name_pre_str,
	const char *	function_name_str,
	const char *	function_name_post_str,
	const char *	line_number_pre_str,
	const int	line_number,
	const char *	line_number_post_str,
	const char *	time_pre_str,
	const char *	time_post_str,
	const char *	backtrace_pre_str,
	const char *	backtrace_post_str,
	const char *	backtrace_entry_pre_str,
	const char *	backtrace_entry_post_str,
	const char *	backtrace_num_pre_str,
	const char *	backtrace_num_post_str,
	const char *	outer_message_pre_str,
	const char *	outer_message_post_str,
	const char *	inner_message_pre_str,
	const char *	inner_message_post_str,

	const char *	field_str,

	const char *	format_str,
	va_list		argument_list
)
{
	//Create variables to keep track of where items should be printed in
	//the destination string

	char * cur_str =	dst_str;
	size_t cur_str_len =	dst_str_len;
	size_t char_tally =	0;


	//Start the debug message with the message pre text

	if (NULL == outer_message_pre_str)
	{
		outer_message_pre_str = "";
	}

	rem_snprintf
	(
		cur_str,
		cur_str_len,
		&cur_str,
		&cur_str_len,
		NULL,
		&char_tally,
		str_truncated_ptr,
		"%s",
		outer_message_pre_str
	);


	//Start iterating through the #field_str, and print each debug message
	//field as needed

	for
	(
		size_t field_str_index = 0;

		('\0' != field_str [field_str_index]);

		field_str_index += 1
	)
	{
		if ('b' == field_str [field_str_index])
		{
			internal_sly_print_backtrace
			(
				cur_str,
				cur_str_len,
				&cur_str,
				&cur_str_len,
				NULL,
				&char_tally,
				str_truncated_ptr,

				complain_flag,
				backtrace_numbered_flag,

				backtrace_max_depth,
				backtrace_trim_depth_low,
				backtrace_trim_depth_high,


				backtrace_pre_str,
				backtrace_post_str,
				backtrace_entry_pre_str,
				backtrace_entry_post_str,
				backtrace_num_pre_str,
				backtrace_num_post_str
			);
		}


		else if ('f' == field_str [field_str_index])
		{
			internal_sly_print_function_name
			(
				cur_str,
				cur_str_len,
				&cur_str,
				&cur_str_len,
				NULL,
				&char_tally,
				str_truncated_ptr,
				function_name_pre_str,
				function_name_str,
				function_name_post_str
			);
		}


		else if ('l' == field_str [field_str_index])
		{
			internal_sly_print_line_number
			(
				cur_str,
				cur_str_len,
				&cur_str,
				&cur_str_len,
				NULL,
				&char_tally,
				str_truncated_ptr,
				line_number_pre_str,
				line_number,
				line_number_post_str
			);
		}


		else if ('s' == field_str [field_str_index])
		{
			internal_sly_print_source_file
			(
				cur_str,
				cur_str_len,
				&cur_str,
				&cur_str_len,
				NULL,
				&char_tally,
				str_truncated_ptr,
				source_file_pre_str,
				source_file_str,
				source_file_post_str
			);
		}


		else if ('t' == field_str [field_str_index])
		{
			internal_sly_print_time
			(
				cur_str,
				cur_str_len,
				&cur_str,
				&cur_str_len,
				NULL,
				&char_tally,
				str_truncated_ptr,
				time_pre_str,
				time_post_str
			);
		}


		else if ('m' == field_str [field_str_index])
		{
			//Print out the actual main message within the debug
			//message

			if (NULL == inner_message_pre_str)
			{
				inner_message_pre_str = "";
			}

			rem_snprintf
			(
				cur_str,
				cur_str_len,
				&cur_str,
				&cur_str_len,
				NULL,
				&char_tally,
				str_truncated_ptr,
				"%s",
				inner_message_pre_str
			);



			//Using a particular #va_list twice in a function cause
			//it to segfault. Since this code block can technically
			//be reached multiple times in the same function call,
			//it is necessary to make and destroy a copy of the
			//#argument_list each time.

			va_list arg_list_copy;
			va_copy (arg_list_copy, argument_list);


			if (NULL == format_str)
			{
				format_str = "";
			}

			rem_vsnprintf
			(
				cur_str,
				cur_str_len,
				&cur_str,
				&cur_str_len,
				NULL,
				&char_tally,
				str_truncated_ptr,
				format_str,
				arg_list_copy
			);


			va_end (arg_list_copy);



			if (NULL == inner_message_post_str)
			{
				inner_message_post_str = "";
			}

			rem_snprintf
			(
				cur_str,
				cur_str_len,
				&cur_str,
				&cur_str_len,
				NULL,
				&char_tally,
				str_truncated_ptr,
				"%s",
				inner_message_post_str
			);
		}
	}


	//End the debug message with the message post text

	if (NULL == outer_message_post_str)
	{
		outer_message_post_str = "";
	}

	rem_snprintf
	(
		cur_str,
		cur_str_len,
		&cur_str,
		&cur_str_len,
		NULL,
		&char_tally,
		str_truncated_ptr,
		"%s",
		outer_message_post_str
	);


	//Make sure to update #rem_str, #rem_str_len, #print_len_ptr, and
	//#tally_chars_ptr, because they were not passed to rem_snprintf()
	//directly

	if (NULL != rem_str_ptr)
	{
		*rem_str_ptr = cur_str;
	}

	if (NULL != rem_str_len_ptr)
	{
		*rem_str_len_ptr = cur_str_len;
	}

	if (NULL != print_len_ptr)
	{
		*print_len_ptr = char_tally;
	}

	if (NULL != tally_chars_ptr)
	{
		*tally_chars_ptr = (*tally_chars_ptr) + char_tally;
	}


	return 0;
}




/// Version of internal_sly_va_print_debug_str() that uses '...' for the
/// variadic arguments.

__attribute__ ((format (printf, 35, 36)))

int internal_sly_print_debug_str
(
	char *		dst_str,
	size_t		dst_str_len,
	char **		rem_str_ptr,
	size_t *	rem_str_len_ptr,
	size_t *	print_len_ptr,
	size_t *	tally_chars_ptr,
	int *		str_truncated_ptr,

	int		complain_flag,

	int		backtrace_numbered_flag,

	int		backtrace_max_depth,
	unsigned int	backtrace_trim_depth_low,
	unsigned int	backtrace_trim_depth_high,

	const char *	source_file_pre_str,
	const char *	source_file_str,
	const char *	source_file_post_str,
	const char *	function_name_pre_str,
	const char *	function_name_str,
	const char *	function_name_post_str,
	const char *	line_number_pre_str,
	const int	line_number,
	const char *	line_number_post_str,
	const char *	time_pre_str,
	const char *	time_post_str,
	const char *	backtrace_pre_str,
	const char *	backtrace_post_str,
	const char *	backtrace_entry_pre_str,
	const char *	backtrace_entry_post_str,
	const char *	backtrace_num_pre_str,
	const char *	backtrace_num_post_str,
	const char *	outer_message_pre_str,
	const char *	outer_message_post_str,
	const char *	inner_message_pre_str,
	const char *	inner_message_post_str,

	const char *	field_str,

	const char *	format_str,
	...
)


{
	va_list argument_list;

	va_start (argument_list, format_str);

	int ret_val =	internal_sly_va_print_debug_str
			(
				dst_str,
				dst_str_len,
				rem_str_ptr,
				rem_str_len_ptr,
				print_len_ptr,
				tally_chars_ptr,
				str_truncated_ptr,

				complain_flag,

				backtrace_numbered_flag,

				backtrace_max_depth,
				backtrace_trim_depth_low,
				backtrace_trim_depth_high,

				source_file_pre_str,
				source_file_str,
				source_file_post_str,
				function_name_pre_str,
				function_name_str,
				function_name_post_str,
				line_number_pre_str,
				line_number,
				line_number_post_str,
				time_pre_str,
				time_post_str,
				backtrace_pre_str,
				backtrace_post_str,
				backtrace_entry_pre_str,
				backtrace_entry_post_str,
				backtrace_num_pre_str,
				backtrace_num_post_str,
				outer_message_pre_str,
				outer_message_post_str,
				inner_message_pre_str,
				inner_message_post_str,

				field_str,

				format_str,
				argument_list
			);

	va_end (argument_list);


	return ret_val;
}




/// Allocates memory for a debug string, and prints a debug message to it.
///
/// All unlisted arguments follow the same conventions as in
/// internal_sly_va_print_debug_str().
///
/// @warning
/// free() MUST be called on #created_str sometime after calling this function
/// to avoid creating a memory leak.
///
/// @param created_str			Where to place a pointer to the created
///					string
///
/// @param created_str_len		Where to place the length of the newly
///					created string
///
/// @param truncated_flag		Where to place a flag that will be TRUE
///					whenever the created string had its
///					length truncated
///
/// @param max_str_len			The maximum length that a debug message
///					can be before it should be truncated
///
/// @param truncation_post_str		A string to print out after truncation
///					occurs (if it occurs at all). The
///					length of this string is not included
///					in #max_str_len.
///
/// @param complain_flag		Flag for whether errors (if any) should
///					be printed to #stderr
///
/// @return				0 upon success
///
///					-1 upon general failure
///
///					-2 if arguments if #created_str,
///					created_str_len, and truncated_flag
///					were all NULL
///
///					-3 if the string would have had 0
///					length

static int internal_sly_va_create_debug_str
(
	char **		created_str,
	size_t *	created_str_len,
	int *		truncated_flag,

	size_t		max_str_len,

	int		complain_flag,

	int		backtrace_numbered_flag,

	int		backtrace_max_depth,
	unsigned int	backtrace_trim_depth_low,
	unsigned int	backtrace_trim_depth_high,

	const char *	source_file_pre_str,
	const char *	source_file_str,
	const char *	source_file_post_str,
	const char *	function_name_pre_str,
	const char *	function_name_str,
	const char *	function_name_post_str,
	const char *	line_number_pre_str,
	const int	line_number,
	const char *	line_number_post_str,
	const char *	time_pre_str,
	const char *	time_post_str,
	const char *	backtrace_pre_str,
	const char *	backtrace_post_str,
	const char *	backtrace_entry_pre_str,
	const char *	backtrace_entry_post_str,
	const char *	backtrace_num_pre_str,
	const char *	backtrace_num_post_str,
	const char *	outer_message_pre_str,
	const char *	outer_message_post_str,
	const char *	inner_message_pre_str,
	const char *	inner_message_post_str,

	const char *	field_str,

	const char *	format_str,
	va_list		argument_list
)
{
	//Check if this function should act as a no-op, because there is no
	//way to return useful information

	if
	(
		(NULL == created_str)		&&
		(NULL == created_str_len)	&&
		(NULL == truncated_flag)
	)
	{
		if (complain_flag)
		{
			fprintf
			(
				stderr,
				"ERROR : %s from %s, %s, %d : "
				"Acted as no-op due to NULL arguments\n",
				__func__,
				(NULL != source_file_str) ? source_file_str : "?",
				(NULL != function_name_str ) ? function_name_str : "?",
				line_number
			);
		}

		return -2;
	}


	//Perform a print attempt _without_ a destination string, just to see
	//what the size of the printed string would be

	size_t str_len =	0;
	int    str_truncated =	0;

	internal_sly_va_print_debug_str
	(
		NULL,
		0,
		NULL,
		NULL,
		&str_len,
		NULL,
		NULL,

		complain_flag,

		backtrace_numbered_flag,

		backtrace_max_depth,
		backtrace_trim_depth_low,
		backtrace_trim_depth_high,

		source_file_pre_str,
		source_file_str,
		source_file_post_str,
		function_name_pre_str,
		function_name_str,
		function_name_post_str,
		line_number_pre_str,
		line_number,
		line_number_post_str,
		time_pre_str,
		time_post_str,
		backtrace_pre_str,
		backtrace_post_str,
		backtrace_entry_pre_str,
		backtrace_entry_post_str,
		backtrace_num_pre_str,
		backtrace_num_post_str,
		outer_message_pre_str,
		outer_message_post_str,
		inner_message_pre_str,
		inner_message_post_str,

		field_str,

		format_str,
		argument_list
	);


	//The value of #str_len returned by internal_sly_va_print_debug_str()
	//does not include the NULL terminator, so make sure to take that into
	//account

	str_len += 1;


	//If the needed string length is longer than #max_str_len, make sure to
	//clip the length

	if (str_len > max_str_len)
	{
		str_len = max_str_len;
	}


	//Check if there is a string to allocate whatsoever. Otherwise, perform
	//early exit of the function

	if (0 == str_len)
	{
		if (NULL != created_str)
		{
			*created_str = "";
		}

		if (NULL != created_str_len)
		{
			*created_str_len = 0;
		}

		if (NULL != truncated_flag)
		{
			*truncated_flag = (0 == max_str_len);
		}


		return -3;
	}


	//Actually allocate a string capable of holding the debug message

	char * str = malloc (sizeof (char) * str_len);

	if (NULL == str)
	{
		if (complain_flag)
		{
			fprintf
			(
				stderr,
				"ERROR : %s from %s, %s, %d : memory "
				"allocation of size %lu bytes failed for #str\n",
				__func__,
				(NULL != source_file_str) ? source_file_str : "?",
				(NULL != function_name_str ) ? function_name_str : "?",
				line_number,
				str_len
			);
		}

		return -1;
	}


	//Print the string, but this time with an actual destination string

	internal_sly_va_print_debug_str
	(
		str,
		str_len,
		NULL,
		NULL,
		NULL,
		NULL,
		&str_truncated,

		complain_flag,

		backtrace_numbered_flag,

		backtrace_max_depth,
		backtrace_trim_depth_low,
		backtrace_trim_depth_high,

		source_file_pre_str,
		source_file_str,
		source_file_post_str,
		function_name_pre_str,
		function_name_str,
		function_name_post_str,
		line_number_pre_str,
		line_number,
		line_number_post_str,
		time_pre_str,
		time_post_str,
		backtrace_pre_str,
		backtrace_post_str,
		backtrace_entry_pre_str,
		backtrace_entry_post_str,
		backtrace_num_pre_str,
		backtrace_num_post_str,
		outer_message_pre_str,
		outer_message_post_str,
		inner_message_pre_str,
		inner_message_post_str,

		field_str,

		format_str,
		argument_list
	);


	//Pass the string to the caller by way of the pointer arguments

	if (NULL != created_str)
	{
		*created_str = str;
	}

	else
	{
		//Make sure to free() the string if the caller can not

		free (str);
	}

	if (NULL != created_str_len)
	{
		*created_str_len = str_len;
	}

	if (NULL != truncated_flag)
	{
		*truncated_flag = str_truncated;
	}


	//Return success

	return 0;
}




/// Version of internal_sly_create_debug_str() that uses '...' for the
/// variadic arguments.
///
/// (Disabled, because this function is currently unused)

/*

static int internal_sly_create_debug_str
(
	char **		created_str,
	size_t *	created_str_len,
	int *		truncated_flag,

	size_t		max_str_len,

	int		complain_flag,

	int		backtrace_numbered_flag,

	int		backtrace_max_depth,
	unsigned int	backtrace_trim_depth_low,
	unsigned int	backtrace_trim_depth_high,

	const char *	source_file_pre_str,
	const char *	source_file_str,
	const char *	source_file_post_str,
	const char *	function_name_pre_str,
	const char *	function_name_str,
	const char *	function_name_post_str,
	const char *	line_number_pre_str,
	const int	line_number,
	const char *	line_number_post_str,
	const char *	time_pre_str,
	const char *	time_post_str,
	const char *	backtrace_pre_str,
	const char *	backtrace_post_str,
	const char *	backtrace_entry_pre_str,
	const char *	backtrace_entry_post_str,
	const char *	backtrace_num_pre_str,
	const char *	backtrace_num_post_str,
	const char *	outer_message_pre_str,
	const char *	outer_message_post_str,
	const char *	inner_message_pre_str,
	const char *	inner_message_post_str,

	const char *	field_str,

	const char *	format_str,
	...
)
{
	va_list argument_list;

	va_start (argument_list, format_str);

	int ret_val =	internal_sly_va_create_debug_str
			(
				created_str,
				created_str_len,
				truncated_flag,

				max_str_len,

				complain_flag,

				backtrace_numbered_flag,

				backtrace_max_depth,
				backtrace_trim_depth_low,
				backtrace_trim_depth_high,

				source_file_pre_str,
				source_file_str,
				source_file_post_str,
				function_name_pre_str,
				function_name_str,
				function_name_post_str,
				line_number_pre_str,
				line_number,
				line_number_post_str,
				time_pre_str,
				time_post_str,
				backtrace_pre_str,
				backtrace_post_str,
				backtrace_entry_pre_str,
				backtrace_entry_post_str,
				backtrace_num_pre_str,
				backtrace_num_post_str,
				outer_message_pre_str,
				outer_message_post_str,
				inner_message_pre_str,
				inner_message_post_str,

				field_str,

				format_str,
				argument_list
			);


	va_end (argument_list);

	return ret_val;
}

*/




/// Prints out a nicely formatted debugging message to #sly_debug_file_fp.
///
/// All unlisted arguments follow the same conventions as in
/// internal_sly_va_print_debug_str().
///
/// @param dst_file_ptr			The #FILE to print the debug message to
///
/// @param use_lock			Flag for whether this function should
///					observe the lock for #sly_debug_file_fp
///					or not
///
/// @param max_str_len			The maximum length that a debug message
///					can be before it should be truncated
///
/// @param truncation_post_str		A string to print out after truncation
///					occurs (if it occurs at all). The
///					length of this string is not included
///					in #max_str_len.
///
/// @param flush_flag			Flag for whether #sly_debug_file_fp
///					should be flushed immediately after the
///					string is printed
///
/// @param complain_flag		Flag for whether errors (if any) should
///					be printed to #stderr
///
/// @return				0 upon success, -1 upon failure

int internal_sly_va_print_debug_file_ptr
(
	FILE *		dst_file_ptr,

	int		use_lock,

	size_t		max_str_len,
	const char *	truncation_post_str,

	int		flush_flag,

	int		complain_flag,

	int		backtrace_numbered_flag,

	int		backtrace_max_depth,
	unsigned int	backtrace_trim_depth_low,
	unsigned int	backtrace_trim_depth_high,

	const char *	source_file_pre_str,
	const char *	source_file_str,
	const char *	source_file_post_str,
	const char *	function_name_pre_str,
	const char *	function_name_str,
	const char *	function_name_post_str,
	const char *	line_number_pre_str,
	const int	line_number,
	const char *	line_number_post_str,
	const char *	time_pre_str,
	const char *	time_post_str,
	const char *	backtrace_pre_str,
	const char *	backtrace_post_str,
	const char *	backtrace_entry_pre_str,
	const char *	backtrace_entry_post_str,
	const char *	backtrace_num_pre_str,
	const char *	backtrace_num_post_str,
	const char *	outer_message_pre_str,
	const char *	outer_message_post_str,
	const char *	inner_message_pre_str,
	const char *	inner_message_post_str,

	const char *	field_str,

	const char *	format_str,
	va_list		argument_list
)
{
	//Sanity check on #dst_file_ptr

	if (NULL == dst_file_ptr)
	{
		if (complain_flag)
		{
			fprintf
			(
				stderr,
				"ERROR : %s from %s, %s, %d : "
				"dst_file_ptr was NULL, so printing "
				"attempt was skipped.\n",
				__func__,
				(NULL != source_file_str) ? source_file_str : "?",
				(NULL != function_name_str ) ? function_name_str : "?",
				line_number
			);
		}

		return -1;

	}


	//Create the debug string

	char * str =		NULL;
	size_t str_len =	0;
	int    str_truncated =	0;

	int ret_val =	internal_sly_va_create_debug_str
			(
				&str,
				&str_len,
				&str_truncated,

				max_str_len,

				complain_flag,

				backtrace_numbered_flag,

				backtrace_max_depth,
				backtrace_trim_depth_low,
				backtrace_trim_depth_high,

				source_file_pre_str,
				source_file_str,
				source_file_post_str,
				function_name_pre_str,
				function_name_str,
				function_name_post_str,
				line_number_pre_str,
				line_number,
				line_number_post_str,
				time_pre_str,
				time_post_str,
				backtrace_pre_str,
				backtrace_post_str,
				backtrace_entry_pre_str,
				backtrace_entry_post_str,
				backtrace_num_pre_str,
				backtrace_num_post_str,
				outer_message_pre_str,
				outer_message_post_str,
				inner_message_pre_str,
				inner_message_post_str,

				field_str,

				format_str,
				argument_list
			);


	if (0 != ret_val)
	{
		if (complain_flag)
		{
			fprintf
			(
				stderr,
				"ERROR : %s from %s, %s, %d : "
				"Could not create debug string.\n",
				__func__,
				(NULL != source_file_str) ? source_file_str : "?",
				(NULL != function_name_str ) ? function_name_str : "?",
				line_number
			);
		}

		return -1;

	}


	//Acquire the global printing lock
	//
	//(Even if not printing to #sly_debug_file_fp, we can not guarantee
	//that no other printing attempt is being made to #dst_file_ptr)

	internal_sly_print_debug_lock_acquire (use_lock);


	//Perform a check on the provided file pointer to make sure it can
	//actually be printed to (Note how this check is done AFTER the lock is
	//acquired, just to make sure no other SlyDebug functions can
	//possibly be messing around with this pointer at this time).

	if (-1 == fileno (dst_file_ptr))
	{
		if (complain_flag)
		{
			fprintf
			(
				stderr,
				"ERROR : %s from %s, %s, %d : "
				"dst_file_ptr = %p referred to a "
				"file in an invalid state. Did this file "
				"have fclose() called on it?\n",
				__func__,
				(NULL != source_file_str) ? source_file_str : "?",
				(NULL != function_name_str ) ? function_name_str : "?",
				line_number,
				dst_file_ptr
			);
		}


		//Deallocate the debug string

		free (str);


		return -1;
	}


	//Print the debug string to the actual file destination

	fprintf (dst_file_ptr, "%s", str);


	//If truncation to the string occurred, make sure to indicate this in
	//the output (if desired).

	if (NULL != truncation_post_str)
	{
		if (str_truncated)
		{
			fprintf (dst_file_ptr, "%s", truncation_post_str);
		}
	}


	//Flush the file (if requested)

	if (flush_flag)
	{
		fflush (dst_file_ptr);
	}


	//Release the global printing lock

	internal_sly_print_debug_lock_release (use_lock);


	//Free the memory used to hold the debug string

	free (str);


	//Return success

	return 0;
}




/// Version of internal_sly_va_print_debug_file_ptr() that uses '...' for the
/// variadic arguments.

__attribute__ ((format (printf, 33, 34)))

int internal_sly_print_debug_file_ptr
(
	FILE *		dst_file_ptr,

	int		use_lock,

	size_t		max_str_len,
	const char *	truncation_post_str,

	int		flush_flag,

	int		complain_flag,

	int		backtrace_numbered_flag,

	int		backtrace_max_depth,
	unsigned int	backtrace_trim_depth_low,
	unsigned int	backtrace_trim_depth_high,

	const char *	source_file_pre_str,
	const char *	source_file_str,
	const char *	source_file_post_str,
	const char *	function_name_pre_str,
	const char *	function_name_str,
	const char *	function_name_post_str,
	const char *	line_number_pre_str,
	const int	line_number,
	const char *	line_number_post_str,
	const char *	time_pre_str,
	const char *	time_post_str,
	const char *	backtrace_pre_str,
	const char *	backtrace_post_str,
	const char *	backtrace_entry_pre_str,
	const char *	backtrace_entry_post_str,
	const char *	backtrace_num_pre_str,
	const char *	backtrace_num_post_str,
	const char *	outer_message_pre_str,
	const char *	outer_message_post_str,
	const char *	inner_message_pre_str,
	const char *	inner_message_post_str,

	const char *	field_str,

	const char *	format_str,
	...
)
{
	va_list argument_list;

	va_start (argument_list, format_str);

	int ret_val =	internal_sly_va_print_debug_file_ptr
			(
				dst_file_ptr,

				use_lock,

				max_str_len,
				truncation_post_str,

				flush_flag,

				complain_flag,

				backtrace_numbered_flag,

				backtrace_max_depth,
				backtrace_trim_depth_low,
				backtrace_trim_depth_high,

				source_file_pre_str,
				source_file_str,
				source_file_post_str,
				function_name_pre_str,
				function_name_str,
				function_name_post_str,
				line_number_pre_str,
				line_number,
				line_number_post_str,
				time_pre_str,
				time_post_str,
				backtrace_pre_str,
				backtrace_post_str,
				backtrace_entry_pre_str,
				backtrace_entry_post_str,
				backtrace_num_pre_str,
				backtrace_num_post_str,
				outer_message_pre_str,
				outer_message_post_str,
				inner_message_pre_str,
				inner_message_post_str,

				field_str,

				format_str,
				argument_list
			);


	va_end (argument_list);

	return ret_val;
}




/// Prints a debug message just like internal_sly_print_debug_file_ptr(),
/// however, this prints to #sly_debug_file_fp, which is the global debug file.

int internal_sly_va_print_debug
(
	int		use_lock,

	size_t		max_str_len,
	const char *	truncation_post_str,

	int		flush_flag,

	int		complain_flag,

	int		backtrace_numbered_flag,

	int		backtrace_max_depth,
	unsigned int	backtrace_trim_depth_low,
	unsigned int	backtrace_trim_depth_high,

	const char *	source_file_pre_str,
	const char *	source_file_str,
	const char *	source_file_post_str,
	const char *	function_name_pre_str,
	const char *	function_name_str,
	const char *	function_name_post_str,
	const char *	line_number_pre_str,
	const int	line_number,
	const char *	line_number_post_str,
	const char *	time_pre_str,
	const char *	time_post_str,
	const char *	backtrace_pre_str,
	const char *	backtrace_post_str,
	const char *	backtrace_entry_pre_str,
	const char *	backtrace_entry_post_str,
	const char *	backtrace_num_pre_str,
	const char *	backtrace_num_post_str,
	const char *	outer_message_pre_str,
	const char *	outer_message_post_str,
	const char *	inner_message_pre_str,
	const char *	inner_message_post_str,

	const char *	field_str,

	const char *	format_str,
	va_list		argument_list
)
{

	//Create the debug string

	char * str =		NULL;
	size_t str_len =	0;
	int    str_truncated =	0;

	int ret_val =	internal_sly_va_create_debug_str
			(
				&str,
				&str_len,
				&str_truncated,

				max_str_len,

				complain_flag,

				backtrace_numbered_flag,

				backtrace_max_depth,
				backtrace_trim_depth_low,
				backtrace_trim_depth_high,

				source_file_pre_str,
				source_file_str,
				source_file_post_str,
				function_name_pre_str,
				function_name_str,
				function_name_post_str,
				line_number_pre_str,
				line_number,
				line_number_post_str,
				time_pre_str,
				time_post_str,
				backtrace_pre_str,
				backtrace_post_str,
				backtrace_entry_pre_str,
				backtrace_entry_post_str,
				backtrace_num_pre_str,
				backtrace_num_post_str,
				outer_message_pre_str,
				outer_message_post_str,
				inner_message_pre_str,
				inner_message_post_str,

				field_str,

				format_str,
				argument_list
			);


	if (0 != ret_val)
	{
		if (complain_flag)
		{
			fprintf
			(
				stderr,
				"ERROR : %s from %s, %s, %d : "
				"Could not create debug string.\n",
				__func__,
				(NULL != source_file_str) ? source_file_str : "?",
				(NULL != function_name_str ) ? function_name_str : "?",
				line_number
			);
		}

		return -1;

	}


	//Acquire the global printing lock

	internal_sly_print_debug_lock_acquire (use_lock);


	//Check if the global debug file ptr is safe to print to

	if (NULL == sly_debug_file_fp)
	{
		if ((0 == sly_debug_file_needs_init) && (complain_flag))
		{
			fprintf
			(
				stderr,
				"WARNING : %s from %s, %s, %d : "
				"The global debug file pointer was NULL, "
				"so it has automatically been set to #stderr.\n",
				__func__,
				(NULL != source_file_str) ? source_file_str : "?",
				(NULL != function_name_str ) ? function_name_str : "?",
				line_number
			);
		}


		//Set the debug file to #stderr

		internal_sly_close_debug_file
		(
			(0),
			complain_flag,

			source_file_str,
			function_name_str,
			line_number
		);

	}


	if (-1 == fileno (sly_debug_file_fp))
	{
		if (complain_flag)
		{
			fprintf
			(
				stderr,
				"ERROR : %s from %s, %s, %d : "
				"#sly_debug_file_fp = %p referred to a "
				"file in an invalid state. Attempting to "
				"fall-back to #stderr. Printing attempt failed.\n",
				__func__,
				(NULL != source_file_str) ? source_file_str : "?",
				(NULL != function_name_str ) ? function_name_str : "?",
				line_number,
				sly_debug_file_fp
			);
		}


		//Set the debug file to #stderr

		internal_sly_close_debug_file
		(
			(0),
			complain_flag,

			source_file_str,
			function_name_str,
			line_number
		);


		//Deallocate the debug string

		free (str);


		return -1;
	}


	//Print the debug string to the global debug file

	fprintf (sly_debug_file_fp, "%s", str);


	//If truncation to the string occurred, make sure to indicate this in
	//the output (if desired).

	if (NULL != truncation_post_str)
	{
		if (str_truncated)
		{
			fprintf (sly_debug_file_fp, "%s", truncation_post_str);
		}
	}


	//Flush the file (if requested)

	if (flush_flag)
	{
		fflush (sly_debug_file_fp);
	}


	//Release the global printing lock

	internal_sly_print_debug_lock_release (use_lock);


	//Free the memory used to hold the debug string

	free (str);


	return ret_val;
}




/// Version of internal_sly_va_print_debug () that uses '...' for the variadic
/// arguments.

__attribute__ ((format (printf, 32, 33)))

int internal_sly_print_debug
(
	int		use_lock,

	size_t		max_str_len,
	const char *	truncation_post_str,

	int		flush_flag,

	int		complain_flag,

	int		backtrace_numbered_flag,

	int		backtrace_max_depth,
	unsigned int	backtrace_trim_depth_low,
	unsigned int	backtrace_trim_depth_high,

	const char *	source_file_pre_str,
	const char *	source_file_str,
	const char *	source_file_post_str,
	const char *	function_name_pre_str,
	const char *	function_name_str,
	const char *	function_name_post_str,
	const char *	line_number_pre_str,
	const int	line_number,
	const char *	line_number_post_str,
	const char *	time_pre_str,
	const char *	time_post_str,
	const char *	backtrace_pre_str,
	const char *	backtrace_post_str,
	const char *	backtrace_entry_pre_str,
	const char *	backtrace_entry_post_str,
	const char *	backtrace_num_pre_str,
	const char *	backtrace_num_post_str,
	const char *	outer_message_pre_str,
	const char *	outer_message_post_str,
	const char *	inner_message_pre_str,
	const char *	inner_message_post_str,

	const char *	field_str,

	const char *	format_str,
	...
)
{
	va_list argument_list;

	va_start (argument_list, format_str);

	int ret_val =	internal_sly_va_print_debug
			(
				use_lock,

				max_str_len,
				truncation_post_str,

				flush_flag,

				complain_flag,

				backtrace_numbered_flag,

				backtrace_max_depth,
				backtrace_trim_depth_low,
				backtrace_trim_depth_high,

				source_file_pre_str,
				source_file_str,
				source_file_post_str,
				function_name_pre_str,
				function_name_str,
				function_name_post_str,
				line_number_pre_str,
				line_number,
				line_number_post_str,
				time_pre_str,
				time_post_str,
				backtrace_pre_str,
				backtrace_post_str,
				backtrace_entry_pre_str,
				backtrace_entry_post_str,
				backtrace_num_pre_str,
				backtrace_num_post_str,
				outer_message_pre_str,
				outer_message_post_str,
				inner_message_pre_str,
				inner_message_post_str,

				field_str,

				format_str,
				argument_list
			);

	va_end (argument_list);


	return ret_val;
}




/// Performs a small test-bench on rem_snprintf(). Basically, a string is
/// printed to again and again using information produced by rem_snprintf().
///
/// This test-bench requires manual validation.

int rem_snprintf_tb ()
{
	size_t str_len =	120;
	char * str =		malloc (sizeof (char) * str_len);

	if (NULL == str)
	{
		return -1;
	}

	size_t rem_str_len =	str_len;
	char * rem_str =	str;

	size_t print_chars =	0;
	size_t tally_chars =	0;
	int    str_truncate =	0;


	for (int i = 0; i < 3 * str_len; i += 1)
	{
		rem_snprintf
		(
			rem_str,
			rem_str_len,
			&rem_str,
			&rem_str_len,
			&print_chars,
			&tally_chars,
			&str_truncate,
			"Print %d : ",
			i
		);

		printf
		(
			"str @            %p\n"
			"str_len =        %lu\n"
			"rem_str @        %p\n"
			"rem_str_len =    %lu\n"
			"print_chars =    %lu\n"
			"tally_chars =	  %lu\n"
			"str_truncate =   %d\n\n"
			"str = \"%s\"\n\n",
			str,
			str_len,
			rem_str,
			rem_str_len,
			print_chars,
			tally_chars,
			str_truncate,
			str
		);
	}

	free (str);

	return 0;
}




/// Performs a small unit test on internal_sly_print_backtrace().
///
/// This testbench checks if the backtrace function successfully completes with
/// semi-random parameters, and requires manual validation.

int internal_sly_print_backtrace_tb (unsigned int recursion_level)
{
	//If not at the desired recursion level, add more functions to the call
	//stack to make the backtrace output more interesting

	const unsigned int DESIRED_RECURSION_LEVEL = 10;

	if (recursion_level < DESIRED_RECURSION_LEVEL)
	{
		return internal_sly_print_backtrace_tb (recursion_level + 1);
	}


	//Allocate a string to hold the backtrace output

	size_t str_len =	1024 * 1024;
	char * str =		malloc (sizeof (char) * str_len);

	if (NULL == str)
	{
		return -1;
	}


	//Print out the current stack trace with different sets of parameters

	int bt_numbered_flag =	1;
	int bt_max_depth =	DESIRED_RECURSION_LEVEL + 10;
	int bt_trim_low =	0;
	int bt_trim_high =	0;

	for (int i = 0; i < 20; i += 1)
	{
		bt_max_depth += (i * ((i % 2) ? 1 : -1));

		bt_trim_low  = ((i / 2));

		bt_trim_high = ((i / 3));


		internal_sly_print_backtrace
		(
			str,
			str_len,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,

			(1),
			bt_numbered_flag,

			bt_max_depth,
			bt_trim_low,
			bt_trim_high,

			"BACKTRACE START\n",
			"BACKTRACE END\n",
			"\t",
			"\n",
			"<",
			"> "
		);

		printf
		(
			"bt_numbered_flag = %d\n"
			"bt_max_depth =     %d\n"
			"bt_trim_low =      %d\n"
			"bt_trim_high =     %d\n"
			"\n",
			bt_numbered_flag,
			bt_max_depth,
			bt_trim_low,
			bt_trim_high
		);

		printf ("%s\n\n", str);
	}


	//Free the string used to hold the backtrace outputs

	free (str);


	return 0;
}




/// Stub function used to make stack trace printed in internal_sly_print_backtrace_tb ()
/// more interesting.

int internal_sly_print_backtrace_tb_stub (int recursion_level)
{
	return internal_sly_print_backtrace_tb (recursion_level);
}




/// Performs a simple test-bench on internal_sly_print_debug_str(), making sure
/// that semi-random debug field options are respected (which has the
/// side-effect of producing hideous debug messages).
///
/// This test-bench requires manual validation.

int internal_sly_print_debug_str_tb ()
{
	//Allocate a string large enough to hold the debug messages

	size_t str_len =	1024 * 1024;

	char * str =		NULL;


	str = malloc (sizeof (char) * str_len);

	if (NULL == str)
	{
		return -1;
	}


	//Iterate through a variety of different printing parameters

	for (int i = 0; i < 64; i += 1)
	{
		//Randomly generate a "field_str" to determine the order in
		//which fields are printed out"

		const size_t field_str_len = 7;

		char field_str [field_str_len];

		for (size_t k = 0; k < (field_str_len - 1); k += 1)
		{
			unsigned int char_sel = rand () % 6;

			if (0 == char_sel)	{field_str [k] = 'b';}

			else if (1 == char_sel)	{field_str [k] = 'f';}

			else if (2 == char_sel)	{field_str [k] = 'l';}

			else if (3 == char_sel)	{field_str [k] = 'm';}

			else if (4 == char_sel)	{field_str [k] = 's';}

			else if (5 == char_sel)	{field_str [k] = 't';}

			else			{field_str [k] = 'm';}

		}

		field_str [field_str_len - 1] = '\0';


		//Alternate between wheter backtraces should be numbered or not

		int backtrace_numbered_flag = (i % 2);


		//Generate some (ugly) debug messages with the randomly
		//selected field strings

		char * rem_str =		NULL;
		size_t rem_str_len =		0;

		internal_sly_print_debug_str
		(
			str,
			str_len,
			&rem_str,
			&rem_str_len,
			NULL,
			NULL,
			NULL,

			1,

			backtrace_numbered_flag,

			1024,
			0,
			0,

			"SRC  <",
			__FILE__,
			"> SRC\n",
			"FUNC <",
			__func__,
			"> FUNC\n",
			"LINE <",
			__LINE__,
			"> LINE\n",
			"TIME <",
			"> TIME\n",
			"BT <\n",
			"> BT\n",
			"[",
			"]\n",
			"(",
			")\t",
			"\n++++++++++++++++++++\n",
			"\n++++++++++++++++++++\n",
			"\n--------------------\n",
			"\n--------------------\n",

			"slftmb",

			"Normal #field_str message! #%d",
			i
		);


		internal_sly_print_debug_str
		(
			rem_str,
			rem_str_len,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,

			1,

			backtrace_numbered_flag,

			1024,
			0,
			0,

			"SRC  <",
			__FILE__,
			"> SRC\n",
			"FUNC <",
			__func__,
			"> FUNC\n",
			"LINE <",
			__LINE__,
			"> LINE\n",
			"TIME <",
			"> TIME\n",
			"BT <\n",
			"> BT\n",
			"[",
			"]\n",
			"(",
			")\t",
			"\n++++++++++++++++++++\n",
			"\n++++++++++++++++++++\n",
			"\n--------------------\n",
			"\n--------------------\n",

			field_str,

			"Message with #field_str = %s, #%d!",
			field_str,
			i
		);

		printf ("%s\n\n", str);
	}


	//Free the memory used on the debug string

	free (str);

	return 0;
}




/// Performs a test-bench on internal_sly_print_debug (), in particular making
/// sure that debug messages are properly truncated when the max debug string
/// length is too small.
///
/// This function requires manual validation.

int internal_sly_print_debug_tb ()
{

	//Intentionally print out strings such that they get truncated

	for (int i = 0; i < 1024; i += 50)
	{
		internal_sly_print_debug
		(
			1,

			i,
			"!!!!TRUNCATION!!!!\n",

			1,

			1,

			1,

			1024,
			0,
			0,

			"SRC  : ",
			__FILE__,
			"\n",
			"FUNC : ",
			__func__,
			"\n",
			"LINE : ",
			__LINE__,
			"\n",
			"TIME : ",
			"\n",
			"BACKTRACE\n",
			"",
			"",
			"\n",
			"(",
			")\t",
			"\n++++++++++\n",
			"++++++++++\n",
			"----------\n",
			"\n----------\n",

			"slftmb",

			"ABCDEFGHIJKLMNOPQRSTUVWXYZ!"
		);
	}


	return 0;
}




/// Performs a test-bench on various file operations available in SlyDebug.
/// The results of this test-bench require manual observation.

int internal_sly_file_op_tb ()
{

	//Enter a loop where a SlyDebug file is repeatedly opened, printed to,
	//and closed.

	for (int i = 0; i < 50; i += 1)
	{
		//Set a Sly debug file, and print to it normally

		internal_sly_set_debug_file
		(
			1,
			1,
			(i == 0),
			"test_debug.txt",

			__FILE__,
			__func__,
			__LINE__
		);

		internal_sly_print_debug
		(
			1,

			1024 * 1024,
			".....",

			1,

			1,

			1,

			1024,
			0,
			0,

			"SRC  : ",
			__FILE__,
			"\n",
			"FUNC : ",
			__func__,
			"\n",
			"LINE : ",
			__LINE__,
			"\n",
			"TIME : ",
			"\n",
			"BACKTRACE\n",
			"",
			"",
			"\n",
			"(",
			")\t",
			"\n++++++++++\n",
			"++++++++++\n",
			"----------\n",
			"\n----------\n",

			"slftmb",

			"Regular Print %d",
			i
		);

		internal_sly_close_debug_file
		(
			1,
			1,

			__FILE__,
			__func__,
			__LINE__
		);


		//Manually set the debug file pointer here, and then
		//print to that file

		FILE * file_ptr = fopen ("test_debug.txt", "a+");

		if (NULL == file_ptr)
		{
			fprintf
			(
				stderr,
				"WARNING : %s : Early test termination due to "
				"NULL FILE pointer\n",
				__func__
			);

			return -1;
		}

		internal_sly_dup_debug_file_ptr
		(
			1,
			1,
			file_ptr,

			__FILE__,
			__func__,
			__LINE__
		);

		internal_sly_print_debug
		(
			1,

			1024 * 1024,
			".....",

			1,

			1,

			1,

			1024,
			0,
			0,

			"SRC  : ",
			__FILE__,
			"\n",
			"FUNC : ",
			__func__,
			"\n",
			"LINE : ",
			__LINE__,
			"\n",
			"TIME : ",
			"\n",
			"BACKTRACE\n",
			"",
			"",
			"\n",
			"(",
			")\t",
			"\n++++++++++\n",
			"++++++++++\n",
			"----------\n",
			"\n----------\n",

			"slftmb",

			"Semi-regular Print %d",
			i
		);


		//Attempt to close the file using the normal SlyDebug
		//function for doing so, but this should NOT close the original
		//file because it was set with internal_sly_dup_debug_file_ptr ()

		internal_sly_close_debug_file
		(
			1,
			1,

			__FILE__,
			__func__,
			__LINE__
		);


		//Attempt a direct print to the debug file which should NOT be
		//closed here

		internal_sly_print_debug_file_ptr
		(
			file_ptr,

			1,

			1024 * 1024,
			".....",

			1,

			1,

			1,

			1024,
			0,
			0,

			"SRC  : ",
			__FILE__,
			"\n",
			"FUNC : ",
			__func__,
			"\n",
			"LINE : ",
			__LINE__,
			"\n",
			"TIME : ",
			"\n",
			"BACKTRACE\n",
			"",
			"",
			"\n",
			"(",
			")\t",
			"\n++++++++++\n",
			"++++++++++\n",
			"----------\n",
			"\n----------\n",

			"slftmb",

			"Direct Print %d",
			i
		);


		//Flush the debug file for good measure

		internal_sly_flush_debug_file
		(
			1,
			1,

			__FILE__,
			__func__,
			__LINE__
		);


		//ACTUALLY close the debug file now

		fclose (file_ptr);
	}


	return 0;
}




/*

int main ()
{
	rem_snprintf_tb ();

	internal_sly_print_backtrace_tb_stub (0);

	internal_sly_print_debug_str_tb ();

	internal_sly_print_debug_tb ();

	internal_sly_file_op_tb ();

	return 0;
}

*/

