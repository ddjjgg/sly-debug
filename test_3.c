/*!*****************************************************************************
 *
 *	@file
 *	test_3.c
 *
 *	Tests if printing out to a given FILE * works. (Note that the default
 *	printing destination is #stderr)
 *
 *****************************************************************************/

#define SLY_PRINT_FIELD_STR		"slfm"

#include "SlyDebug.h"

int main()
{

	SLY_DUP_DEBUG_FILE_PTR (stdout);

	SLY_PRINT_DEBUG
	(
		"This message should up on the #stdout, Furthermore, "
		"this should not have a 'time' field or a backtrace "
		"printout."
	);

	SLY_CLOSE_DEBUG_FILE ();

	return 0;
}
