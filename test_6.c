/*!****************************************************************************
 *
 * 	@file
 *	test_6.c
 *
 *	This file tests if SlyDebug respects redefinitions of various option
 *	macros.
 *
 *****************************************************************************/

#define SLY_MAX_STR_LEN_DEFAULT			((size_t) 600)

#define SLY_PRINT_TRUNCATE_POST_TEXT		"! CUSTOM TRUNCATE STRING !\n"

#define SLY_PRINT_FLUSH_DEFAULT			(0)

#define SLY_COMPLAIN_DEFAULT			(0)

#define SLY_PRINT_BT_NUMBERED_DEFAULT		(3)

#define SLY_PRINT_BT_MAX_DEPTH_DEFAULT		(500)

#define SLY_PRINT_BT_TRIM_LOW_DEFAULT		(1)

#define SLY_PRINT_BT_TRIM_HIGH_DEFAULT		(6)

#define SLY_PRINT_SOURCE_FILE_PRE_TEXT		"src{"
#define SLY_PRINT_SOURCE_FILE_POST_TEXT		"}\n"

#define SLY_PRINT_FUNCTION_PRE_TEXT		"func{"
#define SLY_PRINT_FUNCTION_POST_TEXT		"}\n"

#define SLY_PRINT_LINE_PRE_TEXT			"line{"
#define SLY_PRINT_LINE_POST_TEXT		"}\n"

#define SLY_PRINT_TIME_PRE_TEXT			"time{"
#define SLY_PRINT_TIME_POST_TEXT		"}\n"

#define SLY_PRINT_BT_PRE_TEXT			"\nSTART STACK TRACE\n"
#define SLY_PRINT_BT_POST_TEXT			"STOP STACK TRACE\n"

#define SLY_PRINT_BT_ENTRY_PRE_TEXT		"-("
#define SLY_PRINT_BT_ENTRY_POST_TEXT		")-\n"

#define SLY_PRINT_BT_NUMBER_PRE_TEXT		"<#"
#define SLY_PRINT_BT_NUMBER_POST_TEXT		">\t"

#define SLY_PRINT_INNER_MESSAGE_PRE_TEXT	"-----\n"
#define SLY_PRINT_INNER_MESSAGE_POST_TEXT	"\n-----"

#define SLY_PRINT_OUTER_MESSAGE_PRE_TEXT	"==========\n"
#define SLY_PRINT_OUTER_MESSAGE_POST_TEXT	"==========\n"

#define SLY_PRINT_FIELD_STR			"tflsmb"


#include "SlyDebug.h"


void printing_test ()
{
	SLY_PRINT_DEBUG
	(
		"This printing attempt should demonstrate all of the options "
		"set in this file.\n"
		"SLY_MAX_STR_LEN_DEFAULT        = %zu\n"
		"SLY_PRINT_FLUSH_DEFAULT        = %d\n"
		"SLY_COMPLAIN_DEFAULT           = %d\n"
		"SLY_PRINT_BT_MAX_DEPTH_DEFAULT = %d\n"
		"SLY_PRINT_BT_TRIM_LOW_DEFAULT  = %d\n"
		"SLY_PRINT_BT_TRIM_HIGH_DEFAULT = %d",
		SLY_MAX_STR_LEN_DEFAULT,
		SLY_PRINT_FLUSH_DEFAULT,
		SLY_COMPLAIN_DEFAULT, 
		SLY_PRINT_BT_MAX_DEPTH_DEFAULT, 
		SLY_PRINT_BT_TRIM_LOW_DEFAULT,  
		SLY_PRINT_BT_TRIM_HIGH_DEFAULT 
	);

	return;
}


void stub_2 ()
{
	printing_test ();

	return;
}


void stub_1 ()
{
	stub_2 ();

	return;
}


void stub_0 ()
{
	stub_1 ();

	return;
}


int main()
{
	stub_0 ();

	return 0;
}

