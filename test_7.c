/*!****************************************************************************
 *
 * 	@file
 *	test_7.c
 *
 *	This file tests if SlyDebug stress tests printing / handling debug file
 *	operations in a multi-threaded program correctly
 *
 *****************************************************************************/


#include "stdio.h"
#include "unistd.h"
#include "pthread.h"
#include "SlyDebug.h"




#define NUM_PRINTS	(10000)

#define NUM_FILE_OPS	(10000)




/// A function that prints out "thread 0" multiple times

void * printer_0 (void * finish_flag)
{
	for (int print_num = 0; print_num < NUM_PRINTS; print_num += 1)
	{
		SLY_PRINT_DEBUG_OPT 
		(
		 	"slfm", 
			"thread 0\nthread 0\nthread 0\nthread 0\n"
		);
	}

	* ((int *) finish_flag) = 1;

	return NULL;
}




/// A function that prints out "thread 1" multiple times

void * printer_1 (void * finish_flag)
{
	for (int print_num = 0; print_num < NUM_PRINTS; print_num += 1)
	{
		SLY_PRINT_DEBUG_OPT
		(
			"slfm",
			"thread 1\nthread 1\nthread 1\nthread 1\n"
		);
	}

	* ((int *) finish_flag) = 1;

	return NULL;
}




// A function that rapidly makes use of various SlyDebug file operations

void * arbitrary_file_ops (void * finish_flag)
{
	for (int op_num = 0; op_num < NUM_FILE_OPS; op_num += 1)
	{
		//In this loop, make it difficult as possible for the printing
		//attempts to succeed by attempting to mess with the selected
		//debug file

		SLY_SET_DEBUG_FILE (0, "test_7.txt");

		SLY_CLOSE_DEBUG_FILE ();

		FILE * f_ptr = fopen ("test_7.txt", "a+");

		if (NULL == f_ptr) {continue;}

		SLY_DUP_DEBUG_FILE_PTR (f_ptr);

		fclose (f_ptr);

		SLY_CLOSE_DEBUG_FILE ();

		SLY_FLUSH_DEBUG_FILE ();

		fprintf (stderr, "arbitrary_file_ops () iterated ...\n\n");

	}

	* ((int *) finish_flag) = 1;

	return NULL;
}




int main()
{
	SLY_SET_DEBUG_FILE (1, "test_7.txt");

	volatile int finish_flag_0 = 0;
	volatile int finish_flag_1 = 0;
	volatile int finish_flag_2 = 0;
	volatile int finish_flag_3 = 0;

	pthread_t thread_0;
	pthread_t thread_1;
	pthread_t thread_2;
	pthread_t thread_3;

	pthread_create (&thread_0, NULL, printer_0, (void *) &finish_flag_0);
	pthread_create (&thread_1, NULL, printer_1, (void *) &finish_flag_1);
	pthread_create (&thread_2, NULL, arbitrary_file_ops, (void *) &finish_flag_2);
	pthread_create (&thread_3, NULL, arbitrary_file_ops, (void *) &finish_flag_3);

	while
	(
		(! finish_flag_0) || (! finish_flag_1) ||
		(! finish_flag_2) || (! finish_flag_3)
	)
	{
		; //Wait for the printing functions to finish
	}

	pthread_join (thread_0, NULL);
	pthread_join (thread_1, NULL);
	pthread_join (thread_2, NULL);
	pthread_join (thread_3, NULL);


	return 0;
}

