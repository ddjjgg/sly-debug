/*!****************************************************************************
 *
 * 	@file
 *	test_9.c
 *
 *	This file exhaustively calls every function macro in SlyDebug.h to see
 *	if they work as expected
 *
 *****************************************************************************/


#define SLY_DEBUG_ENABLE	(1)

#include "SlyDebug.h"

int main()
{
	//Test all of the SlyDebug file macro's

	SLY_DUP_DEBUG_FILE_PTR (stdout);

	SLY_PRINT_DEBUG ("Print #0");

	SLY_SET_DEBUG_FILE (1, "./test_9.txt");

	SLY_PRINT_DEBUG ("Print #1");

	SLY_FLUSH_DEBUG_FILE ();

	SLY_CLOSE_DEBUG_FILE ();


	//Test all of the SLY_PRINT_DEBUG* macros

	SLY_PRINT_DEBUG ("Print #2");

	SLY_PRINT_DEBUG_OPT ("slftm", "Print #3");

	SLY_PRINT_DEBUG_ALL (	256,
				"...TRUNCATE\n\n",
				1,
				1,
				1,
				1024,
				0,
				0,
				"",
				__FILE__,
				"@",
				"",
				__func__,
				"@",
				"",
				__LINE__,
				"@",
				"",
				"",
				"BT START\n",
				"BT END\n",
				"",
				"\n",
				"(",
				") ",
				"+++++\n",
				"-----\n\n",
				"\n\n",
				"\n\n",
				"slftmb",

				"Print #4");
	
	
	//Test all of the SLY_PRINT_FP_DEBUG* macros

	FILE * f_ptr = fopen ("./test_9.txt", "a+");

	if (NULL == f_ptr) {return -1;}


	SLY_PRINT_DEBUG_FP (f_ptr, "Print #5");

	SLY_PRINT_DEBUG_FP_OPT (f_ptr, "slftm", "Print #6");

	SLY_PRINT_DEBUG_FP_ALL (	f_ptr,
					256,
					"...TRUNCATE\n\n",
					1,
					1,
					1,
					1024,
					0,
					0,
					"",
					__FILE__,
					"@",
					"",
					__func__,
					"@",
					"",
					__LINE__,
					"@",
					"",
					"",
					"BT START\n",
					"BT END\n",
					"",
					"\n",
					"(",
					") ",
					"+++++\n",
					"-----\n\n",
					"\n\n",
					"\n\n",
					"slftmb",

					"Print #7");


	fclose (f_ptr);


	//Test the remaining macros
	
	SLY_PRINT_IF ((1), "Print #8");
	SLY_PRINT_IF ((0), "Print #9");

	SLY_PRINT_IF_FP (stdout, (1), "Print #10");
	SLY_PRINT_IF_FP (stdout, (0), "Print #11");

	SLY_NULL_CHECK (stdout);
	SLY_NULL_CHECK (NULL);

	SLY_NULL_CHECK_FP (stdout, stdout);
	SLY_NULL_CHECK_FP (stdout, NULL);


	return 0;
}
