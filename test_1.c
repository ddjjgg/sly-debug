/*!****************************************************************************
 *
 * 	@file
 *	test_1.c
 *
 *	This file just tests if SlyDebug can properly print out a
 *	debug message, but to a separate file this time.
 *
 *****************************************************************************/

#include "SlyDebug.h"

int main()
{
	SLY_SET_DEBUG_FILE (1, "test_1.txt");

	SLY_PRINT_DEBUG ("This message should print to the file \"test_1.txt\"");

	SLY_CLOSE_DEBUG_FILE ();

	return 0;
}
