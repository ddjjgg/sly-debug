# SlyDebug

SlyDebug is a library and set of C macros that are intended to help debug C
programs. The design of SlyDebug is intended to require as few modifications in
the calling code as possible, while producing as few logical side-effects as
possible. This makes SlyDebug nearly trivial to add/remove from a project.

SlyDebug provides these major features:

* Thread-safe debug printing macro's, with optional headers and stack
  traces

* The ability to easily specify an output file to print debug messages to,
  whether by an already opened FILE * handle or by a file path

* Upon disabling the debug macro's, the macro's become logically
  equivalent to no-op's, EXCEPT in the case that the macro arguments
  have side-effects. These side-effects are preserved so that entire
  classes of Heisenbugs are avoided.

* The ability to customize the format of debugging messages through #define
  directives

* SlyDebug does not use an initialization or de-initialization function, so it
  always available to use at any part in a program

* Maintains its own internal state so that the calling program does not have to
  pass around debugging-related information itself. (This generally means that
  the calling program does not have to change any of its own function
  interfaces, because there is no SlyDebug * handle to pass around!)


## Building and Installation

To build and install SlyDebug, simply run these commands in the root of the
source directory:

	make;
	sudo make install;

By doing this, the SlyDebug headers will be installed to "/usr/include" and
"/usr/lib" respectively. Alternatively, just 'make' can be run, and then
"SlyDebug.h" and "libSlyDebug**.so" can be manually copied to their
desired locations.

SlyDebug is a semantically versioned library, and so the compiled library
contains a version number. As a matter of convenience, SlyDebug creates
symbolic links to the most recently installed version of the header/library. As
an example, if the version of SlyDebug is A.B.C, then symbolic links will
be installed like so

	/usr/include/SlyDebug-A-B-x.h -> /usr/include/SlyDebug-A-B-C.h
	/usr/include/SlyDebug-A-x-x.h -> /usr/include/SlyDebug-A-B-C.h

	/usr/lib/libSlyDebug-A-B-x.so -> /usr/lib/libSlyDebug-A-B-C.so
	/usr/lib/libSlyDebug-A-x-x.so -> /usr/lib/libSlyDebug-A-B-C.so

By doing this, programs can optionally compile against a more generic version
of the library, thus increasing the chances that a user has a compatible
version already installed.


## Usage without Installation

It is trivial to use SlyDebug in a project without installation. Simply
copy the files "SlyDebug.h" and "SlyDebug.c" into the other project's
directory, and compile the project with them included. The library is small
enough to make this technique practical.

Of course, the drawback of this method is that the project will not
automatically receive bug-fixes when the user updates the library.


## Example Usage

Here is a very simple code example making use of SlyDebug


	//File : test.c


	//This can be uncommented to completely disable SlyDebug.

	//#define SLY_DEBUG_ENABLE   (0)


	#include <SlyDebug-0-x-x.h>


	int some_function (int a, int b)
	{
		int c = a + b;

		SLY_PRINT_DEBUG ("The value of C is %d", c);

		return c;
	}


	int main ()
	{
		//This creates a debug file for debug messages to go to
		//
		//(If this statement is commented out, the debug message will
		//go to #stderr instead)

		SLY_SET_DEBUG_FILE (1, "./debug_log.txt");


		some_function (2, 3);


		//This closes the debug file, if one is open.
		//
		//Note that by default SlyDebug will flush the file after every
		//debug message, so even if this were left out, the debug
		//messages would still be saved.

		SLY_CLOSE_DEBUG_FILE ();

		return 0;
	}


When this program is compiled with...


	gcc -rdynamic -lSlyDebug-0-x-x ./test.c -o ./test


...and then executed, it produces this message like so in "./debug_log.txt"...


	./test.c @ some_function() @ line 16		Tue Apr 21 11:24:08 2020

	The value of C is 5

	---------- Backtrace ----------
	<0>	./test(some_function+0x11e) [0x5623152a92cf]
	<1>	./test(main+0x5c) [0x5623152a9337]
	<2>	/usr/lib/libc.so.6(__libc_start_main+0xf3) [0x7f6cd4684023]
	<3>	./test(_start+0x2e) [0x5623152a908e]


## Other info

* The stack traces in SlyDebug are capable of providing actual
  function names when the program is compiled with the "-rdynamic" flag
  (assuming the compiler is 'gcc'). It is recommended to use this flag
  when compiling debug builds of your program.


## To-do

* This project will not enter version 1.0.0 until a larger set of
  test-benches is developed

* A proper "./configure" script should be provided

