/*!****************************************************************************
 *
 * 	@file
 *	test_2.c
 *
 *	This file just tests if the SLY_PRINT_FIELD_STR macro works properly
 *	for SlyDebug.
 *
 *****************************************************************************/


#define SLY_PRINT_FIELD_STR		"sflmb"

#define SLY_PRINT_TIME_PRE_TEXT		"Time: "

#include "SlyDebug.h"


int main()
{
	SLY_PRINT_DEBUG
	(
		"This message should show up on #stderr, and it should "
		"not have a \"time\" field."
	);

	return 0;
}
