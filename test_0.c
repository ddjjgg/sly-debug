/*!****************************************************************************
 *
 *	@file
 *	test_0.c
 *
 *	This file just tests if SlyDebug can properly print out a debug
 *	message.
 *
 *****************************************************************************/

#include "SlyDebug.h"

int main()
{
	SLY_PRINT_DEBUG ("This message should show up on #stderr.");

	return 0;
}
