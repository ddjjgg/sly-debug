/*!****************************************************************************
 *
 * 	@file
 *	test_5.c
 *
 *	This file just tests if the SLY_PRINT_IF() macro works correctly.
 *	Additionally, it tests setting some configuration options for the debug
 *	macros.
 *
 *****************************************************************************/


#define SLY_PRINT_SOURCE_FILE_PRE_TEXT		"src{"
#define SLY_PRINT_SOURCE_FILE_POST_TEXT		"}\n"

#define SLY_PRINT_FUNCTION_PRE_TEXT		"func{"
#define SLY_PRINT_FUNCTION_POST_TEXT		"}\n"

#define SLY_PRINT_LINE_PRE_TEXT			"line{"
#define SLY_PRINT_LINE_POST_TEXT		"}\n"

#define SLY_PRINT_TIME_PRE_TEXT			"time{"
#define SLY_PRINT_TIME_POST_TEXT		"}\n"

#define SLY_PRINT_BT_PRE_TEXT			"\nSTART STACK TRACE\n"
#define SLY_PRINT_BT_POST_TEXT			"STOP STACK TRACE\n"

#define SLY_PRINT_BT_ENTRY_PRE_TEXT		"-("
#define SLY_PRINT_BT_ENTRY_POST_TEXT		")-\n"

#define SLY_PRINT_BT_NUMBER_PRE_TEXT		"<#"
#define SLY_PRINT_BT_NUMBER_POST_TEXT		">\t"

#define SLY_PRINT_INNER_MESSAGE_PRE_TEXT	"-----\n"
#define SLY_PRINT_INNER_MESSAGE_POST_TEXT	"\n-----"

#define SLY_PRINT_OUTER_MESSAGE_PRE_TEXT	"==========\n"
#define SLY_PRINT_OUTER_MESSAGE_POST_TEXT	"==========\n"


#include "SlyDebug.h"


void printing_test ()
{
	for (int ii = 0; ii< 10; ii++)
	{	
		SLY_PRINT_IF ((ii % 2) == 0, "Test Print #%d", ii);
	}

	return;
}

int main()
{

	SLY_PRINT_IF (1, "This message SHOULD be printed out.");

	SLY_PRINT_IF (0, "This message should NOT be printed out.");

	SLY_PRINT_IF (1, "This message SHOULD also be printed out.");

	SLY_PRINT_IF_FP (stdout, 1, "This message SHOULD be printed out.");

	SLY_PRINT_IF_FP (stdout, 0, "This message should NOT be printed out.");

	SLY_PRINT_IF_FP (stdout, 1, "This message SHOULD also be printed out.");

	printing_test ();

	return 0;
}

