/*!****************************************************************************
 *
 * 	@file
 *	test_8.c
 *
 *	This file tests some edge-cases for the SlyDebug file operations
 *
 *****************************************************************************/


//FIXME : This test-bench demonstrates a minor resource leak when tested with
//valgrind. It is known that it happens right after SLY_SET_DEBUG_FILE () is
//used on the same file SLY_DUP_DEBUG_FILE_PTR () copied a (FILE *) from.
//Inside SLY_SET_DEBUG_FILE (), however, it should close that (FILE *) before
//opening said file again. The leak is ~500 bytes, originating from a call to
//malloc () in fdopen () in internal_sly_dup_debug_file_ptr ().
//
//The cause of this resource leak is currently not understood, but it doesn't
//appear to be a problem in practice. The same leak occurs in "test_7.c" where
//several thousand instances of SLY_DUP_DEBUG_FILE_PTR () followed by
//SLY_SET_DEBUG_FILE () are executed, but only ~1500 bytes in total are leaked.
//Notably, 3 threads in total call SLY_SET_DEBUG_FILE (), and obviously
//(3 * 500) = 1500. So the leak does not appear to "build-up".
//
//Additionally, no other program I've written using SlyDebug has demonstrated
//this leak either.
//
//Even if it's not a practical problem, I would still like to resolve this for
//the sake of cleansliness.


#include "stdio.h"
#include "SlyDebug.h"


int main ()
{
	//Test if printing works on a standard stream when it is closed

	SLY_DUP_DEBUG_FILE_PTR (stdout);

	fclose (stdout);

	SLY_PRINT_DEBUG ("This should print out on #stdout.");

	SLY_FLUSH_DEBUG_FILE ();

	SLY_CLOSE_DEBUG_FILE ();


	//Tests if printing to a file works if that file is closed externally
	
	FILE * f_ptr = fopen ("test_8.txt", "w+");

	if (NULL != f_ptr)
	{
		SLY_DUP_DEBUG_FILE_PTR (f_ptr);

		fclose (f_ptr);

		SLY_PRINT_DEBUG ("This should print to test_8.txt");

		SLY_FLUSH_DEBUG_FILE ();

		SLY_CLOSE_DEBUG_FILE ();
	}


	//Tests if being handed NULL file pointer is handled correctly

	SLY_DUP_DEBUG_FILE_PTR (NULL);


	//Tests if switching between SLY_DUP_DEBUG_FILE_PTR () to
	//SLY_SET_DEBUG_FILE () works gracefully

	f_ptr = fopen ("test_8.txt", "a+");

	if (NULL != f_ptr)
	{
		SLY_DUP_DEBUG_FILE_PTR (f_ptr);

		SLY_SET_DEBUG_FILE (0, "test_8.txt");

		fclose (f_ptr);

		SLY_PRINT_DEBUG ("This should also print to test_8.txt");

		SLY_FLUSH_DEBUG_FILE ();

		SLY_CLOSE_DEBUG_FILE ();
	}


	//Tests if being set to an impossible file is handled gracefully
	
	SLY_SET_DEBUG_FILE (1, "/////");

	SLY_PRINT_DEBUG ("This should print out on #stderr.");

	SLY_CLOSE_DEBUG_FILE ();

	return 0;
}
