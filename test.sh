#!/bin/bash

#set -o xtrace




#Check if this is being executed in the correct directory.

DIR_ERROR=\
"\nERROR: Could not find both './makefile' and './SlyDebug.h'; make sure "\
"to run this script in the top of the SlyDebug project directory.\n\n"


if  [ ! -f "./makefile" ] || [ ! -f "./SlyDebug.h" ]
then
	printf "$DIR_ERROR"
	
	exit;
fi




#Make sure that the LD_LIBRARY_PATH variable includes the current directory, so
#that the library that will be built in this directory will be referenced by
#the built test executables

LD_LIBRARY_PATH=$(pwd):$LD_LIBRARY_PATH

echo $LD_LIBRARY_PATH

export LD_LIBRARY_PATH




#Ask user if they want to continue, and then compile the unit files

CONT_PROMPT=\
"\nThis script will re-run 'make' before performing the unit tests, "\
"and delete old debug logs.\n"\
"Continue? (y/n)\n\n";

printf "$CONT_PROMPT"


read CONTINUE;

if [[ "${CONTINUE,,}" != "y" ]]
then
	printf "Exiting...\n";

	exit;
fi

printf "\n";


make || exit 1;

printf "\n";




#Checks for the debug files to clear.

FOUND_DEBUG_FILES=$( ls -1 | grep test\.\*.txt )

if [[ -n $FOUND_DEBUG_FILES ]]
then
	printf	"The following debug logs will be deleted:\n\n";

	printf	"$FOUND_DEBUG_FILES\n\n";

	rm --preserve-root -v $FOUND_DEBUG_FILES;

	printf "\n";
fi




#test_0.o
#
#Tests if anything gets printed out whatsoever

RESULTS_0=$( ./test_0.o 2>&1);  

if [[ -z $RESULTS_0 ]]
then
	printf "test_0.o failed, no debug output printed.\n";

	exit;
fi

printf "test_0.o passed.\n";




#test_1.o
#
#Tests if printing to a file works

RESULTS_1=$( ./test_1.o 2>&1);

if [[ ! -s test_1.txt ]]
then
	printf "test_1.o failed, could not find debug output in \"test_1.txt\".\n";

	exit;
fi

printf "test_1.o passed.\n";




#test_2.o
#
#Tests if disabling the time field in the debug messages works

RESULTS_2=$( ./test_2.o 2>&1 );

RESULTS_2=$( echo $RESULTS_2 | grep "Time:");

if [[ -n $RESULTS_2 ]]
then
	printf "test_2.o failed, time field not removed from debug output.\n";

	exit;
fi

printf "test_2.o passed.\n";




#test_3.o
#
#Tests if printing to an arbitrary file pointer works.

RESULTS_3=$( ./test_3.o );

if [[ -s "test_3.txt" || -z "$RESULTS_3" ]]
then
	printf "test_3.o failed, output originally meant for #stderr was not redirected to #stdout.\n"

	exit;
fi

printf "test_3.o passed.\n";




#test_4.o
#
#Tests if it is possible to disable all debugging functionality.

RESULTS_4=$( ./test_4.o 2>&1 );

if [[ -s "test_4.txt" ]]
then
	printf "test_4.o failed, test_4.txt exists\n";

	exit;
fi


if [[ -n $( echo "$RESULTS_4" | grep "FAIL") ]]
then
	printf "test_4.o failed, SlyDebug did not preserve function side-effects when disabled.\n"

	exit
fi


printf "test_4.o passed.\n";




#test_5.o
#
#Tests if the SLY_PRINT_IF macro works, and whether many of the *_PRE/*_POST
#option macros are actually honored

RESULTS_5=$( ./test_5.o 2>&1 );

if [[ -n $( echo "$RESULTS_5" | grep "NOT" ) ]]
then
	printf "test_5.o failed, line with \"NOT\" in it appeared on #stderr.\n"

	exit;
fi

TEST_5_REQUIRED_STRINGS[0]="src{.*}"
TEST_5_REQUIRED_STRINGS[1]="func{.*}"
TEST_5_REQUIRED_STRINGS[2]="line{.*}"
TEST_5_REQUIRED_STRINGS[3]="time{.*}"
TEST_5_REQUIRED_STRINGS[4]="START STACK TRACE"
TEST_5_REQUIRED_STRINGS[5]="STOP STACK TRACE"
TEST_5_REQUIRED_STRINGS[6]="-----"
TEST_5_REQUIRED_STRINGS[7]="=========="

for index in `seq 0 7`
do
	line="${TEST_5_REQUIRED_STRINGS[$index]}"

	if [[ -z $( echo "$RESULTS_5" | grep -- "$line" ) ]]
	then
		printf "test_5.o failed on \"$line\"\n"

		exit;
	fi
done

printf "test_5.o passed.\n";




#test_6.o
#
#Tests if setting another set of SlyDebug parameters works correctly

#See if the file was created after executing ./test_6.o

RESULTS_6=$( ./test_6.o 2>&1 )

TEST_6_REQUIRED_STRINGS[0]="SLY_MAX_STR_LEN_DEFAULT        = 600"
TEST_6_REQUIRED_STRINGS[1]="SLY_PRINT_FLUSH_DEFAULT        = 0"
TEST_6_REQUIRED_STRINGS[2]="SLY_COMPLAIN_DEFAULT           = 0"
TEST_6_REQUIRED_STRINGS[3]="SLY_PRINT_BT_MAX_DEPTH_DEFAULT = 500"
TEST_6_REQUIRED_STRINGS[4]="SLY_PRINT_BT_TRIM_LOW_DEFAULT  = 1"
TEST_6_REQUIRED_STRINGS[5]="SLY_PRINT_BT_TRIM_HIGH_DEFAULT = 6"

for index in `seq 0 5`
do
	line="${TEST_6_REQUIRED_STRINGS[$index]}"

	if [[ -z $( echo "$RESULTS_6" | grep "$line" ) ]]
	then
		printf "test_6.o failed on \"$line\"\n"

		exit;
	fi
done

printf "test_6.o passed.\n";




#test_7.o
#
#Tests if printing by multiple threads works correctly, which is to say that
#contents from one printing attempt do not interfere with printing attempts
#from other threads

RESULTS_7=$( ./test_7.o 2>&1 )

t0_count=0;
t1_count=0;

line_count=0;

while read -r line
do
	line_count=$(($line_count + 1));

	#printf "$line_count\n"

	if [[ $line == "thread 0" ]]
	then
		t0_count=$(($t0_count + 1))

		if [ $t0_count == "4" ]
		then
			t0_count=0;	
		fi
	fi

	if [[ $line == "thread 1" ]]
	then
		t1_count=$(($t1_count + 1))

		if [ $t1_count == "4" ]
		then
			t1_count=0;	
		fi
	fi

	if [[ $t0_count != "0" ]] && [[ $t1_count != "0" ]]
	then
		printf "test_7.o failed at line $line_count because "
		printf "printing attempts from 2 threads have been mixed.\n"

		exit
	fi

done < "./test_7.txt"

printf "test_7.o passed.\n";




#test_8.o
#
#Tests a few edge cases of the SlyDebug file operations

RESULTS_8=$( ./test_8.o 2>&1 )

#printf "$RESULTS_8"

NEEDED_STR[0]="This should print out on #stdout.";
NEEDED_STR[1]="This should print out on #stderr.";
NEEDED_STR[2]="Instructed to set the debug file pointer to NULL";
NEEDED_STR[3]="Could not open file \"/////\" to save debug messages.";


for index in `seq 0 3`
do
	FOUND_STR=$( echo $RESULTS_8 | grep "${NEEDED_STR[$index]}");

	#printf "Found $FOUND_STR\n\n"

	if [[ -z $FOUND_STR ]]
	then
		printf "test_8.o failed, could not find \"${NEEDED_STR[$index]}\"\n\n";
	
		exit;
	fi

done


printf "test_8.o passed.\n";




#test_9.o
#
#Tests if every print statement that shows up in the intended place

RESULTS_9=$( ./test_9.o 2>&1 )

#printf "$RESULTS_9"

for index in 0 2 3 4 8 10
do
	NEEDED_STR="Print #$index"

	FOUND_STR=$( echo $RESULTS_9 | grep "$NEEDED_STR");

	#printf "Found $FOUND_STR\n\n"

	if [[ -z $FOUND_STR ]]
	then
		printf "test_9.o failed, could not find \"$NEEDED_STR\"\n\n";
	
		exit;
	fi

done


RESULTS_9=$( cat ./test_9.txt )

for index in 1 5 6 7
do
	NEEDED_STR="Print #$index"

	FOUND_STR=$( echo $RESULTS_9 | grep "$NEEDED_STR");

	#printf "Found $FOUND_STR\n\n"

	if [[ -z $FOUND_STR ]]
	then
		printf "test_9.o failed, could not find \"$NEEDED_STR\"\n\n";
	
		exit;
	fi

done


printf "test_9.o passed.\n";




#test_10.o
#
#Tests if it is possible to disable all debugging functionality exhaustively
#for every debug macro (but we don't check for side-effects in this test-case)

RESULTS_10=$( ./test_10.o 2>&1 );


if [[ -s "test_10.txt" ]]
then
	printf "test_10.o failed, test_10.txt exists\n";

	exit;
fi

if [[ -n $RESULTS_10 ]]
then
	printf "test_10.o failed, debug printing statements still executed.\n";

	exit;
fi


printf "test_10.o passed.\n";



printf "\nAll tests passed successfully.\n\n";

