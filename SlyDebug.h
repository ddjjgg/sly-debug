/*!*****************************************************************************
 *
 * @file
 * SlyDebug.h
 *
 * This library heavily abuses macros to create utilities that help debug other
 * code. In particular, the provided macros focus on debugging in such a way
 * that debugging code can be completely removed from non-debug builds of a
 * program.
 *
 *
 * <b> KNOWN DESIGN QUIRKS </b>
 *
 * 1)
 * 	Many of the function macros in this file are wrapped in do-while
 * 	structures. The main goal of doing this is not to make the macro
 * 	require a ';' (though that is useful). It simply disallows the macro's
 * 	from being used as rvalues. The motivation for this is: What value
 * 	should a macro return when debugging macros are disabled? Should it
 * 	always return success? Is this potential pathway for heisenbugs worth
 * 	it? For now, they will be disallowed as rvalues, because that is the
 * 	easier decision to reverse if need be.
 *
 * 2)
 * 	internal_sly_print_debug_str () is not wrapped in a macro. This is
 * 	because it is only useful for its returned values, so this function is
 * 	outside of the normal debugging macro paradigm.
 *
 * 3)
 *	The debugging canaries are not configurable, unlike the debug printing
 *	macro's. The goal is to make the canaries as unlikely to fail as
 *	possible. (This method is not perfect though; the canaries are useless
 *	when #stderr is closed).
 *
 *****************************************************************************/




#include <stdio.h>




#ifndef SLY_DEBUG_H
#define SLY_DEBUG_H




  /// @defgroup		Sly_Debug_Public_Constants
  ///
  /// @{


  //FIXME: Currently, the "makefile" references these values in this header
  //by way of grep'ing for it. So, by messing up these values, it is too easy
  //to break the build system. Find a better way to store the version numbers,
  //such that these #define's are still available for reading, and so that the
  //makefile properly labels things the same version _always_.


  /// The major version of the current SlyDebug header

  #define SLY_DEBUG_MAJOR_VERSION			(0)


  /// The minor version of the current SlyDebug header

  #define SLY_DEBUG_MINOR_VERSION			(8)


  /// The patch version of the current SlyDebug header

  #define SLY_DEBUG_PATCH_VERSION			(0)


  /// The semantic version of the current SlyDebug header

  #define SLY_DEBUG_SEM_VERSION				"0.8.0"


  /// @}








  /// @defgroup		Master_Sly_Debug_Enable_Options
  ///
  /// @{


  #ifndef SLY_DEBUG_ENABLE

	/// If defined as 0, this disables all of SlyDebug, and removes
	/// it from compiled code

	#define SLY_DEBUG_ENABLE			(1)

  #endif //SLY_DEBUG_ENABLE


  #ifndef SLY_DEBUG_CANARY_ENABLE

	/// If 1, then every SlyDebug macro will use a so-called "debug
	/// canary", which is a _separate statement_ that prints out a
	/// message to #stderr indicating which macro is about to execute.
	///
	/// Why is this useful? The debugging canary will print out before any
	/// of the macro argument's side-effects occur.
	///
	/// Consider that for any C function whatsoever, the arguments could be
	/// the result of another function call. If the arguments to a
	/// debugging macro contain function calls that cause the program to
	/// crash, this can be used to determine which macro contains the
	/// offending function call.
	///
	/// It is highly recommended to leave this option off unless debug
	/// macro arguments are the suspected cause of a program crash. Even
	/// then, it's probably easier to just use a tool like "catchsegv".
	///
	/// @warning
	/// If this is 1, the canaries will still print even if
	/// #SLY_DEBUG_ENABLE is 0.
	///
	/// Remember, SlyDebug preserves debugging macro argument side-effects
	/// even when disabled, So, macro arguments that have side-effects will
	/// not be removed from the compiled program. So such bugs can not be
	/// caught by setting #SLY_DEBUG_ENABLE to 0 alone.

	#define SLY_DEBUG_CANARY_ENABLE			(0)

  #endif


  /// @}








  /// @defgroup		Sly_Debug_Options
  ///
  /// @{


  #ifndef SLY_NULL_CHECK_ENABLE

	/// If 1, then the SlyDebug macros SLY_NULL_CHECK () and
	/// SLY_NULL_CHECK_FP () will be enabled.

	#define SLY_NULL_CHECK_ENABLE			(1)

  #endif //SLY_NULL_CHECK_ENABLE


  #ifndef SLY_MAX_STR_LEN_DEFAULT

	/// The default max string length for a single debug message. Debug
	/// messages with more characters than this will be truncated.
	///
	/// The default only allows for debug messages to be 1 MB each.
	///
	/// This mechanism essentially prevents a single badly formatted
	/// debugging message (e.g. missing a NULL terminator) from consuming
	/// all of the space available for the debug log, whether that is in
	/// RAM or in the file-system.

	#define SLY_MAX_STR_LEN_DEFAULT			((size_t) 1024 * 1024)

  #endif //SLY_MAX_STR_LEN_DEFAULT


  #ifndef SLY_PRINT_TRUNCATE_POST_TEXT

  	/// A string that should be printed after a debug message is
	/// truncated for being too long.
	///
	/// This string is NOT considered part of the debug message's total
	/// length.

  	#define SLY_PRINT_TRUNCATE_POST_TEXT		".....\n\n"

  #endif //SLY_PRINT_TRUNCATE_POST_TEXT


  #ifndef SLY_PRINT_FLUSH_DEFAULT

  	/// If 1, then SlyDebug should flush the output file after every
	/// printed debug message by default.
	///
	/// This is very useful in saving the debug log automatically without
	/// having to call SLY_FLUSH_DEBUG_FILE ().

  	#define SLY_PRINT_FLUSH_DEFAULT			(1)

  #endif //SLY_PRINT_FLUSH_DEFAULT


  #ifndef SLY_COMPLAIN_DEFAULT

  	/// If 1, then internal SlyDebug error messages will be
	/// printed to #stderr by default

  	#define SLY_COMPLAIN_DEFAULT			(1)

  #endif //SLY_COMPLAIN_DEFAULT


  #ifndef SLY_PRINT_BT_NUMBERED_DEFAULT

	/// If 1, then backtrace entries will be numbered by default

	#define SLY_PRINT_BT_NUMBERED_DEFAULT		(1)

  #endif //SLY_PRINT_BT_NUMBERED_DEFAULT


  #ifndef SLY_PRINT_BT_MAX_DEPTH_DEFAULT

  	/// The maximum number of entries that should appear in a backtrace
	/// before functions should be truncated from the list

  	#define SLY_PRINT_BT_MAX_DEPTH_DEFAULT		(1024)

  #endif //SLY_PRINT_BT_MAX_DEPTH_DEFAULT


  #ifndef SLY_PRINT_BT_TRIM_LOW_DEFAULT

	/// The number of least recent functions to trim from the backtrace

  	#define SLY_PRINT_BT_TRIM_LOW_DEFAULT		(0)

  #endif //SLY_PRINT_BT_TRIM_LOW_DEFAULT


  #ifndef SLY_PRINT_BT_TRIM_HIGH_DEFAULT

	/// The number of most recent functions to trim from the backtrace.
	///
	/// The default value here prevents internal SlyDebug functions
	/// from appearing in the debug message

  	#define SLY_PRINT_BT_TRIM_HIGH_DEFAULT		(5)

  #endif //SLY_PRINT_BT_TRIM_HIGH_DEFAULT


  #ifndef SLY_PRINT_SOURCE_FILE_PRE_TEXT

  	/// Determines what will printed right before printing
  	/// the source file name in debug messages.

  	#define SLY_PRINT_SOURCE_FILE_PRE_TEXT		""

  #endif //SLY_PRINT_SOURCE_FILE_PRE_TEXT


  #ifndef SLY_PRINT_SOURCE_FILE_POST_TEXT

  	/// Determines what will be printed right after the source
  	/// file name in debug messages.

  	#define SLY_PRINT_SOURCE_FILE_POST_TEXT		" @ "

  #endif //SLY_PRINT_SOURCE_FILE_POST_TEXT


  #ifndef SLY_PRINT_FUNCTION_PRE_TEXT

  	/// Determines what will printed right before printing
  	/// the function name in debug messages.

  	#define SLY_PRINT_FUNCTION_PRE_TEXT		""

  #endif //SLY_PRINT_FUNCTION_PRE_TEXT


  #ifndef SLY_PRINT_FUNCTION_POST_TEXT

  	/// Determines what will be printed right after the function
  	/// name in debug messages.

  	#define SLY_PRINT_FUNCTION_POST_TEXT		"() @ "

  #endif //SLY_PRINT_FUNCTION_POST_TEXT


  #ifndef SLY_PRINT_LINE_PRE_TEXT

  	/// Determines what will printed right before printing
  	/// the line number in debug messages.

  	#define SLY_PRINT_LINE_PRE_TEXT			"line "

  #endif //SLY_PRINT_LINE_PRE_TEXT


  #ifndef SLY_PRINT_LINE_POST_TEXT

  	/// Determines what will be printed right after the line
  	/// number in debug messages.

  	#define SLY_PRINT_LINE_POST_TEXT		"\t\t"

  #endif //SLY_PRINT_LINE_POST_TEXT


  #ifndef SLY_PRINT_TIME_PRE_TEXT

  	/// Determines what will printed right before printing
  	/// the time in debug messages.

  	#define SLY_PRINT_TIME_PRE_TEXT			""

  #endif //SLY_PRINT_TIME_PRE_TEXT


  #ifndef SLY_PRINT_TIME_POST_TEXT

  	/// Determines what will be printed right after the time
	/// in debug messages.

  	#define SLY_PRINT_TIME_POST_TEXT		""

  #endif //SLY_PRINT_TIME_POST_TEXT


  #ifndef SLY_PRINT_BT_PRE_TEXT

  	/// Determines what will be printed right before a backtrace
  	/// in debug messages.

  	#define SLY_PRINT_BT_PRE_TEXT			"---------- Backtrace ----------\n"

  #endif //SLY_PRINT_BT_HEADER


  #ifndef SLY_PRINT_BT_POST_TEXT

  	/// Determines what will be printed right after a backtrace
  	/// in debug messages.

  	#define SLY_PRINT_BT_POST_TEXT			""

  #endif //SLY_PRINT_BT_POST_TEXT


  #ifndef SLY_PRINT_BT_ENTRY_PRE_TEXT

  	/// Determines what will be printed right before a backtrace entry
  	/// in debug messages.

  	#define SLY_PRINT_BT_ENTRY_PRE_TEXT		""

  #endif //SLY_PRINT_BT_ENTRY_HEADER


  #ifndef SLY_PRINT_BT_ENTRY_POST_TEXT

  	/// Determines what will be printed right after a backtrace entry
  	/// in debug messages.

  	#define SLY_PRINT_BT_ENTRY_POST_TEXT		"\n"

  #endif //SLY_PRINT_BT_ENTRY_POST_TEXT


  #ifndef SLY_PRINT_BT_NUMBER_PRE_TEXT

  	/// Determines what will be printed right before a backtrace
  	/// entry number in debug messages.

  	#define SLY_PRINT_BT_NUMBER_PRE_TEXT		"<"

  #endif //SLY_PRINT_BT_NUMBER_PRE_TEXT


  #ifndef SLY_PRINT_BT_NUMBER_POST_TEXT

  	/// Determines what will be printed right after a backtrace
  	/// entry number in debug messages.

  	#define SLY_PRINT_BT_NUMBER_POST_TEXT		">\t"

  #endif //SLY_PRINT_BT_NUMBER_POST_TEXT


  #ifndef SLY_PRINT_OUTER_MESSAGE_PRE_TEXT

  	/// Defines a string that will be printed before the entire
  	/// debug message every time.

  	#define SLY_PRINT_OUTER_MESSAGE_PRE_TEXT	""

  #endif //SLY_PRINT_OUTER_MESSAGE_PRE_TEXT


  #ifndef SLY_PRINT_OUTER_MESSAGE_POST_TEXT

  	/// Defines a string that will be printed after the entire
  	/// debug message every time.

  	#define SLY_PRINT_OUTER_MESSAGE_POST_TEXT	"\n\n"

  #endif //SLY_PRINT_OUTER_MESSAGE_POST_TEXT


  #ifndef SLY_PRINT_INNER_MESSAGE_PRE_TEXT

  	/// Defines a string that will be printed before the main text
  	/// in debug messages.

  	#define SLY_PRINT_INNER_MESSAGE_PRE_TEXT	"\n\n"

  #endif //SLY_PRINT_INNER_MESSAGE_PRE_TEXT


  #ifndef SLY_PRINT_INNER_MESSAGE_POST_TEXT

  	/// Defines a string that will be printed at the end of the
  	/// main text in debug messages.

  	#define SLY_PRINT_INNER_MESSAGE_POST_TEXT	"\n\n"

  #endif //SLY_PRINT_INNER_MESSAGE_POST_TEXT


  #ifndef SLY_PRINT_FIELD_STR

	/// A string that determines what fields appear in the debug message
	/// output, and in what order.
	///
	/// The following characters have the following sematic meanings:
	///
	/// 	b -> Backtrace Field
	///
	///	f -> Function Name Field
	///
	///	l -> Line Number Field
	///
	///	m -> Message Field
	///
	///	s -> Source File Field
	///
	///	t -> Time Field
	///
	/// See internal_sly_va_print_debug_str() for details.

	#define SLY_PRINT_FIELD_STR			"sfltmb"

  #endif //SLY_PRINT_FIELD_STR


  /// @}








  /// @defgroup		Internal_Sly_Debug_Macros
  ///
  /// @{


  /// This macro is used to help disable other macros. See
  /// #internal_sly_no_op() for details.

  #define SLY_NO_OP(...) \
	\
	do {\
	\
	  internal_sly_no_op((0), ##__VA_ARGS__);\
	\
	} while (0)




  #if (SLY_DEBUG_CANARY_ENABLE)


	/// This macro is used to produce "debug canaries", which indicate what
	/// debugging statement is about to be executed.
	///
	/// Why is this useful? If one of the arguments to a debugging function
	/// (in particular, a function call) have side-effects that cause a
	/// program to crash, this can be used detect where that has happened.

	#define SLY_DEBUG_CANARY(debug_func_str) \
		  \
		  do {\
		  \
		     fprintf\
		     (\
		   	stderr, \
		   	"%s, %s, %d : Canary for %s\n", \
		   	__FILE__, \
		   	__func__, \
		   	__LINE__, \
		   	debug_func_str \
		     );\
		  \
		  } while (0)


  #else //SLY_DEBUG_CANARY_ENABLE


	  #define SLY_DEBUG_CANARY(debug_func_str) \
		  \
		  do {\
		  \
		    SLY_NO_OP(debug_func_str);\
		  \
		  } while (0)


  #endif //SLY_DEBUG_CANARY_ENABLE


  /// @}








  /// @defgroup		Sly_Debug_Function_Macro_Definitions
  ///
  /// @{


  #if (SLY_DEBUG_ENABLE)


	/// Duplicates an open (FILE *), and uses that as the new destination
	/// for SlyDebug printing attempts. This WILL safely close the current
	/// SlyDebug destination file if necessary.
	///
	/// After #file_ptr is duplicated by this function, fclose () may be
	/// used on #file_ptr safely.
	///
	/// Notably, this can be used to set the destination (FILE *) to
	/// #stdout and #stderr.
	///
	/// @warning
	/// #file_ptr must point to an _open_ FILE
	///
	/// @param file_ptr		A (FILE *) for an _open_ file to
	///				duplicate and use as the SlyDebug
	///				destination file

	#define SLY_DUP_DEBUG_FILE_PTR(file_ptr) \
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_DUP_DEBUG_FILE_PTR");\
		  \
		  internal_sly_dup_debug_file_ptr \
		  (\
		  	(1),\
		  	(SLY_COMPLAIN_DEFAULT),\
		  	(file_ptr),\
		  	__FILE__,\
		   	__func__,\
		  	__LINE__\
		  );\
		\
		} while (0)


	/// Sets the new destination for SlyDebug printing attempts using a
	/// file path. This WILL safely close the current SlyDebug destination
	/// file if necessary.
	///
	/// @param truncate_flag	An (int) which, if it is 1, orders the
	///				file contents to be erased when the
	///				file is opened.
	///
	///				This is useful for keeping a debug log
	///				to a reasonable length.
	///
	/// @param file_path		A (const char *) representing the file
	///				path to use as the SlyDebug destination
	///				file

	#define SLY_SET_DEBUG_FILE(truncate_flag, file_path) \
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_SET_DEBUG_FILE");\
		  \
		  internal_sly_set_debug_file \
		  (\
		 	(1),\
		 	(SLY_COMPLAIN_DEFAULT),\
		 	(truncate_flag),\
		 	(file_path),\
		 	__FILE__,\
		  	__func__,\
		 	__LINE__\
		  );\
		\
		} while (0)


	/// Forces the SlyDebug destination file to be flushed (i.e. saves the
	/// contents printed to the current debug file).

	#define SLY_FLUSH_DEBUG_FILE() \
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_FLUSH_DEBUG_FILE");\
		  \
		  internal_sly_flush_debug_file \
		  (\
		  	(1),\
		  	(SLY_COMPLAIN_DEFAULT),\
		  	__FILE__,\
		   	__func__,\
		  	__LINE__\
		  );\
		\
		} while (0)


	/// Closes the current SlyDebug destination file, and then sets it to
	/// #stderr.
	///
	/// To be technically correct, this should be called before main ()
	/// returns in any program that has previously called
	/// SLY_DUP_DEBUG_FILE_PTR () or SLY_SET_DEBUG_FILE (), or a small
	/// resource leak will occur.
	///
	/// Note that both functions automatically close the old SlyDebug
	/// destination file before setting it to a new file. Calling
	/// SLY_CLOSE_DEBUG_FILE () between their calls is therefore
	/// unnecessary.
	///
	/// So as a side-effect, in the worst case, a leak from leaving exactly
	/// one (FILE *) open will occur if SLY_CLOSE_DEBUG_FILE () is not used
	/// before main () returns.

	#define SLY_CLOSE_DEBUG_FILE() \
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_CLOSE_DEBUG_FILE");\
		  \
		  internal_sly_close_debug_file \
		  (\
		  	(1),\
		  	(SLY_COMPLAIN_DEFAULT),\
		  	__FILE__,\
		   	__func__,\
		  	__LINE__\
		  );\
		\
		} while (0)


	/// Prints out a debug message to the current SlyDebug destination
	/// file.
	///
	/// @param format_str		A (const char *) format string
	///				following the same conventions as
	///				printf ()
	///
	/// @param ...			The arguments referenced by #format_str

	#define SLY_PRINT_DEBUG(format_str, ...) \
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_PRINT_DEBUG");\
		  \
		  internal_sly_print_debug\
		  (\
		   	(1),\
		  	\
		  	(SLY_MAX_STR_LEN_DEFAULT),\
		  	(SLY_PRINT_TRUNCATE_POST_TEXT),\
		  	\
		  	(SLY_PRINT_FLUSH_DEFAULT),\
		  	\
		  	(SLY_COMPLAIN_DEFAULT),\
		  	\
		  	(SLY_PRINT_BT_NUMBERED_DEFAULT),\
		  	\
		  	(SLY_PRINT_BT_MAX_DEPTH_DEFAULT),\
		  	(SLY_PRINT_BT_TRIM_LOW_DEFAULT),\
		  	(SLY_PRINT_BT_TRIM_HIGH_DEFAULT),\
		  	\
		  	(SLY_PRINT_SOURCE_FILE_PRE_TEXT),\
		  	__FILE__,\
		  	(SLY_PRINT_SOURCE_FILE_POST_TEXT),\
		  	(SLY_PRINT_FUNCTION_PRE_TEXT),\
		  	__func__,\
		  	(SLY_PRINT_FUNCTION_POST_TEXT),\
		  	(SLY_PRINT_LINE_PRE_TEXT),\
		  	__LINE__,\
		  	(SLY_PRINT_LINE_POST_TEXT),\
		  	(SLY_PRINT_TIME_PRE_TEXT),\
		  	(SLY_PRINT_TIME_POST_TEXT),\
		  	(SLY_PRINT_BT_PRE_TEXT),\
		  	(SLY_PRINT_BT_POST_TEXT),\
		  	(SLY_PRINT_BT_ENTRY_PRE_TEXT),\
		  	(SLY_PRINT_BT_ENTRY_POST_TEXT),\
		  	(SLY_PRINT_BT_NUMBER_PRE_TEXT),\
		  	(SLY_PRINT_BT_NUMBER_POST_TEXT),\
		  	(SLY_PRINT_OUTER_MESSAGE_PRE_TEXT),\
		  	(SLY_PRINT_OUTER_MESSAGE_POST_TEXT),\
		  	(SLY_PRINT_INNER_MESSAGE_PRE_TEXT),\
		  	(SLY_PRINT_INNER_MESSAGE_POST_TEXT),\
		  	\
		  	(SLY_PRINT_FIELD_STR),\
		  	\
		  	(format_str),\
		  	##__VA_ARGS__\
		  );\
		\
		} while (0)


	/// Prints out a debug message to the current SlyDebug destination file
	/// (with control over what fields appear)
	///
	/// @param field_str		A (const char *) "field string" that
	///				determines what fields to include in
	///				the debug message.
	///
	///				This argument follows the same
	///				conventions as #SLY_PRINT_FIELD_STR.
	///
	/// @param format_str		A (const char *) format string
	///				following the same conventions as
	///				printf ()
	///
	/// @param ...			The arguments referenced by #format_str

	#define SLY_PRINT_DEBUG_OPT(	field_str,\
					format_str,\
					...)\
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_PRINT_DEBUG_OPT");\
		  \
		  internal_sly_print_debug\
		  (\
		   	(1),\
		  	\
		  	(SLY_MAX_STR_LEN_DEFAULT),\
		  	(SLY_PRINT_TRUNCATE_POST_TEXT),\
		  	\
		  	(SLY_PRINT_FLUSH_DEFAULT),\
		  	\
		  	(SLY_COMPLAIN_DEFAULT),\
		  	\
		  	(SLY_PRINT_BT_NUMBERED_DEFAULT),\
		  	\
		  	(SLY_PRINT_BT_MAX_DEPTH_DEFAULT),\
		  	(SLY_PRINT_BT_TRIM_LOW_DEFAULT),\
		  	(SLY_PRINT_BT_TRIM_HIGH_DEFAULT),\
		  	\
		  	(SLY_PRINT_SOURCE_FILE_PRE_TEXT),\
		  	__FILE__,\
		  	(SLY_PRINT_SOURCE_FILE_POST_TEXT),\
		  	(SLY_PRINT_FUNCTION_PRE_TEXT),\
		  	__func__,\
		  	(SLY_PRINT_FUNCTION_POST_TEXT),\
		  	(SLY_PRINT_LINE_PRE_TEXT),\
		  	__LINE__,\
		  	(SLY_PRINT_LINE_POST_TEXT),\
		  	(SLY_PRINT_TIME_PRE_TEXT),\
		  	(SLY_PRINT_TIME_POST_TEXT),\
		  	(SLY_PRINT_BT_PRE_TEXT),\
		  	(SLY_PRINT_BT_POST_TEXT),\
		  	(SLY_PRINT_BT_ENTRY_PRE_TEXT),\
		  	(SLY_PRINT_BT_ENTRY_POST_TEXT),\
		  	(SLY_PRINT_BT_NUMBER_PRE_TEXT),\
		  	(SLY_PRINT_BT_NUMBER_POST_TEXT),\
		  	(SLY_PRINT_OUTER_MESSAGE_PRE_TEXT),\
		  	(SLY_PRINT_OUTER_MESSAGE_POST_TEXT),\
		  	(SLY_PRINT_INNER_MESSAGE_PRE_TEXT),\
		  	(SLY_PRINT_INNER_MESSAGE_POST_TEXT),\
		  	\
		  	(field_str),\
		  	\
		  	(format_str),\
		  	##__VA_ARGS__\
		  );\
		\
		} while (0)


	/// Prints out a debug message to the current SlyDebug destination file
	/// (with manual control over all available options).
	///
	/// For information about the parameters, see the documentation for
	/// internal_sly_print_debug ().

	#define SLY_PRINT_DEBUG_ALL(	max_str_len,\
					truncation_post_str,\
					\
					flush_flag,\
					\
					complain_flag,\
					\
					backtrace_numbered_flag,\
					\
					backtrace_max_depth,\
					backtrace_trim_depth_low,\
					backtrace_trim_depth_high,\
					\
					source_file_pre_str,\
					source_file_str,\
					source_file_post_str,\
					function_name_pre_str,\
					function_name_str,\
					function_name_post_str,\
					line_number_pre_str,\
					line_number,\
					line_number_post_str,\
					time_pre_str,\
					time_post_str,\
					backtrace_pre_str,\
					backtrace_post_str,\
					backtrace_entry_pre_str,\
					backtrace_entry_post_str,\
					backtrace_num_pre_str,\
					backtrace_num_post_str,\
					outer_message_pre_str,\
					outer_message_post_str,\
					inner_message_pre_str,\
					inner_message_post_str,\
					\
					field_str,\
					\
					format_str,\
					...)\
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_PRINT_DEBUG_ALL");\
		  \
		  internal_sly_print_debug\
		  (\
			(1),\
			\
			(max_str_len),\
			(truncation_post_str),\
			\
			(flush_flag),\
			\
			(complain_flag),\
			\
			(backtrace_numbered_flag),\
			\
			(backtrace_max_depth),\
			(backtrace_trim_depth_low),\
			(backtrace_trim_depth_high),\
			\
			(source_file_pre_str),\
			(source_file_str),\
			(source_file_post_str),\
			(function_name_pre_str),\
			(function_name_str),\
			(function_name_post_str),\
			(line_number_pre_str),\
			(line_number),\
			(line_number_post_str),\
			(time_pre_str),\
			(time_post_str),\
			(backtrace_pre_str),\
			(backtrace_post_str),\
			(backtrace_entry_pre_str),\
			(backtrace_entry_post_str),\
			(backtrace_num_pre_str),\
			(backtrace_num_post_str),\
			(outer_message_pre_str),\
			(outer_message_post_str),\
			(inner_message_pre_str),\
			(inner_message_post_str),\
			\
			(field_str),\
			\
			(format_str),\
			##__VA_ARGS__\
		  );\
		\
		} while (0)


	/// Prints out a debug message to a given open (FILE *).
	///
	/// This is useful for printing to arbitrary files without modifiying
	/// the current SlyDebug destination file.
	///
	/// @warning
	/// #file_ptr must point to an _open_ FILE
	///
	/// @param file_ptr		A (FILE *) for an open file that will
	///				be the destination for this debug
	///				message.
	///
	/// @param format_str		A (const char *) format string
	///				following the same conventions as
	///				printf ()
	///
	/// @param ...			The arguments referenced by #format_str

	#define SLY_PRINT_DEBUG_FP(file_ptr, format_str, ...) \
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_PRINT_DEBUG_FP");\
		  \
		  internal_sly_print_debug_file_ptr\
		  (\
		   	(file_ptr),\
			\
		   	(1),\
		  	\
		  	(SLY_MAX_STR_LEN_DEFAULT),\
		  	(SLY_PRINT_TRUNCATE_POST_TEXT),\
		  	\
		  	(SLY_PRINT_FLUSH_DEFAULT),\
		  	\
		  	(SLY_COMPLAIN_DEFAULT),\
		  	\
		  	(SLY_PRINT_BT_NUMBERED_DEFAULT),\
		  	\
		  	(SLY_PRINT_BT_MAX_DEPTH_DEFAULT),\
		  	(SLY_PRINT_BT_TRIM_LOW_DEFAULT),\
		  	(SLY_PRINT_BT_TRIM_HIGH_DEFAULT),\
		  	\
		  	(SLY_PRINT_SOURCE_FILE_PRE_TEXT),\
		  	__FILE__,\
		  	(SLY_PRINT_SOURCE_FILE_POST_TEXT),\
		  	(SLY_PRINT_FUNCTION_PRE_TEXT),\
		  	__func__,\
		  	(SLY_PRINT_FUNCTION_POST_TEXT),\
		  	(SLY_PRINT_LINE_PRE_TEXT),\
		  	__LINE__,\
		  	(SLY_PRINT_LINE_POST_TEXT),\
		  	(SLY_PRINT_TIME_PRE_TEXT),\
		  	(SLY_PRINT_TIME_POST_TEXT),\
		  	(SLY_PRINT_BT_PRE_TEXT),\
		  	(SLY_PRINT_BT_POST_TEXT),\
		  	(SLY_PRINT_BT_ENTRY_PRE_TEXT),\
		  	(SLY_PRINT_BT_ENTRY_POST_TEXT),\
		  	(SLY_PRINT_BT_NUMBER_PRE_TEXT),\
		  	(SLY_PRINT_BT_NUMBER_POST_TEXT),\
		  	(SLY_PRINT_OUTER_MESSAGE_PRE_TEXT),\
		  	(SLY_PRINT_OUTER_MESSAGE_POST_TEXT),\
		  	(SLY_PRINT_INNER_MESSAGE_PRE_TEXT),\
		  	(SLY_PRINT_INNER_MESSAGE_POST_TEXT),\
		  	\
		  	(SLY_PRINT_FIELD_STR),\
		  	\
		  	(format_str),\
		  	##__VA_ARGS__\
		  );\
		\
		} while (0)


	/// Prints out a debug message to a given open FILE * (with
	/// control over what fields appear).
	///
	/// This is useful for printing to arbitrary files without modifiying
	/// the current SlyDebug destination file.
	///
	/// @warning
	/// #file_ptr must point to an _open_ FILE
	///
	/// @param file_ptr		A (FILE *) for an open file that will
	///				be the destination for this debug
	///				message.
	///
	/// @param field_str		A (const char *) "field string" that
	///				determines what fields to include in
	///				the debug message.
	///
	///				This argument follows the same
	///				conventions as #SLY_PRINT_FIELD_STR.
	///
	/// @param format_str		A (const char *) format string
	///				following the same conventions as
	///				printf ()
	///
	/// @param ...			The arguments referenced by #format_str

	#define SLY_PRINT_DEBUG_FP_OPT(	file_ptr,\
					field_str,\
					format_str,\
					...)\
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_PRINT_DEBUG_FP_OPT");\
		  \
		  internal_sly_print_debug_file_ptr\
		  (\
		   	(file_ptr),\
			\
		   	(1),\
		  	\
		  	(SLY_MAX_STR_LEN_DEFAULT),\
		  	(SLY_PRINT_TRUNCATE_POST_TEXT),\
		  	\
		  	(SLY_PRINT_FLUSH_DEFAULT),\
		  	\
		  	(SLY_COMPLAIN_DEFAULT),\
		  	\
			(SLY_PRINT_BT_NUMBERED_DEFAULT),\
		  	\
		  	(SLY_PRINT_BT_MAX_DEPTH_DEFAULT),\
		  	(SLY_PRINT_BT_TRIM_LOW_DEFAULT),\
		  	(SLY_PRINT_BT_TRIM_HIGH_DEFAULT),\
		  	\
		  	(SLY_PRINT_SOURCE_FILE_PRE_TEXT),\
		  	__FILE__,\
		  	(SLY_PRINT_SOURCE_FILE_POST_TEXT),\
		  	(SLY_PRINT_FUNCTION_PRE_TEXT),\
		  	__func__,\
		  	(SLY_PRINT_FUNCTION_POST_TEXT),\
		  	(SLY_PRINT_LINE_PRE_TEXT),\
		  	__LINE__,\
		  	(SLY_PRINT_LINE_POST_TEXT),\
		  	(SLY_PRINT_TIME_PRE_TEXT),\
		  	(SLY_PRINT_TIME_POST_TEXT),\
		  	(SLY_PRINT_BT_PRE_TEXT),\
		  	(SLY_PRINT_BT_POST_TEXT),\
		  	(SLY_PRINT_BT_ENTRY_PRE_TEXT),\
		  	(SLY_PRINT_BT_ENTRY_POST_TEXT),\
		  	(SLY_PRINT_BT_NUMBER_PRE_TEXT),\
		  	(SLY_PRINT_BT_NUMBER_POST_TEXT),\
		  	(SLY_PRINT_OUTER_MESSAGE_PRE_TEXT),\
		  	(SLY_PRINT_OUTER_MESSAGE_POST_TEXT),\
		  	(SLY_PRINT_INNER_MESSAGE_PRE_TEXT),\
		  	(SLY_PRINT_INNER_MESSAGE_POST_TEXT),\
		  	\
		  	(field_str),\
		  	\
		  	(format_str),\
		  	##__VA_ARGS__\
		  );\
		\
		} while (0)


	/// Prints out a debug message to a given open (FILE *) (with manual
	/// control over all available options).
	///
	/// This is useful for printing to arbitrary files without modifiying
	/// the current SlyDebug destination file.
	///
	/// @warning
	/// #file_ptr must point to an _open_ FILE
	///
	/// @param file_ptr		A (FILE *) for an open file that will
	///				be the destination for this debug
	///				message.
	///
	/// For information about the parameters, see the documentation for
	/// internal_sly_print_debug ().

	#define SLY_PRINT_DEBUG_FP_ALL(	file_ptr,\
					\
					max_str_len,\
					truncation_post_str,\
					\
					flush_flag,\
					\
					complain_flag,\
					\
					backtrace_numbered_flag,\
					\
					backtrace_max_depth,\
					backtrace_trim_depth_low,\
					backtrace_trim_depth_high,\
					\
					source_file_pre_str,\
					source_file_str,\
					source_file_post_str,\
					function_name_pre_str,\
					function_name_str,\
					function_name_post_str,\
					line_number_pre_str,\
					line_number,\
					line_number_post_str,\
					time_pre_str,\
					time_post_str,\
					backtrace_pre_str,\
					backtrace_post_str,\
					backtrace_entry_pre_str,\
					backtrace_entry_post_str,\
					backtrace_num_pre_str,\
					backtrace_num_post_str,\
					outer_message_pre_str,\
					outer_message_post_str,\
					inner_message_pre_str,\
					inner_message_post_str,\
					\
					field_str,\
					\
					format_str,\
					...)\
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_PRINT_DEBUG_FP_ALL");\
		  \
		  internal_sly_print_debug_file_ptr\
		  (\
		   	(file_ptr),\
			\
			(1),\
			\
			(max_str_len),\
			(truncation_post_str),\
			\
			(flush_flag),\
			\
			(complain_flag),\
			\
			(backtrace_numbered_flag),\
			\
			(backtrace_max_depth),\
			(backtrace_trim_depth_low),\
			(backtrace_trim_depth_high),\
			\
			(source_file_pre_str),\
			(source_file_str),\
			(source_file_post_str),\
			(function_name_pre_str),\
			(function_name_str),\
			(function_name_post_str),\
			(line_number_pre_str),\
			(line_number),\
			(line_number_post_str),\
			(time_pre_str),\
			(time_post_str),\
			(backtrace_pre_str),\
			(backtrace_post_str),\
			(backtrace_entry_pre_str),\
			(backtrace_entry_post_str),\
			(backtrace_num_pre_str),\
			(backtrace_num_post_str),\
			(outer_message_pre_str),\
			(outer_message_post_str),\
			(inner_message_pre_str),\
			(inner_message_post_str),\
			\
			(field_str),\
			\
			(format_str),\
			##__VA_ARGS__\
		  );\
		\
		} while (0)


	/// Prints out a debug message if the given condition is true.
	///
	/// @param condition		Any expression that evaluates to true
	///				or false
	///
	/// @param format_str		A (const char *) argument representing
	///				a format string to print out
	///
	/// @param ...			The variadic arguments referenced by
	///				#format_str (if any)
	///
	/// @warning
	/// Pay close attention to how this macro expands before editing this.
	/// Note how the arguments only have their side-effects happen once no
	/// no matter what. Further note that ##__VA_ARGS is special, and it
	/// must be passed to SLY_NO_OP to maintain its side-effects. (To avoid
	/// this, maybe this macro should just expand to a function call like
	/// the others?)

	#define SLY_PRINT_IF(condition, format_str, ...) \
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_PRINT_IF");\
		  \
		  \
		  int arg_condition = (0 != (condition));\
		  \
		  const char * arg_format_str = (format_str);\
		  \
		  \
		  if (arg_condition)\
		  {\
		  	SLY_PRINT_DEBUG\
		  	(\
		  		arg_format_str,\
		  		##__VA_ARGS__\
		  	);\
		  }\
		  \
		  else\
		  {\
		  	SLY_NO_OP\
		  	(\
		  		arg_format_str,\
		  		##__VA_ARGS__\
		  	);\
		  }\
		\
		} while (0)


	/// Version of SLY_PRINT_IF () that accepts an additional (FILE *)
	/// argument.
	///
	/// Prints out a debug message to an open file if the given condition
	/// is true.
	///
	/// @param file			A (FILE *) for an open file, which the
	///				debug message will be printed to
	///
	/// @param condition		Any expression that evaluates to true
	///				or false
	///
	/// @param format_str		A (const char *) argument representing
	///				a format string to print out
	///
	/// @param ...			The variadic arguments referenced by
	///				#format_str (if any)
	///
	/// @warning
	/// Pay close attention to how this macro expands before editing this.
	/// Note how the arguments only have their side-effects happen once no
	/// no matter what. Further note that ##__VA_ARGS is special, and it
	/// must be passed to SLY_NO_OP to maintain its side-effects. (To avoid
	/// this, maybe this macro should just expand to a function call like
	/// the others?)

	#define SLY_PRINT_IF_FP(file, condition, format_str, ...) \
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_PRINT_IF_FP");\
		  \
		  \
		  FILE * arg_file = (file);\
		  \
		  int arg_condition = (0 != (condition));\
		  \
		  const char * arg_format_str = (format_str);\
		  \
		  \
		  if (arg_condition)\
		  {\
		  	SLY_PRINT_DEBUG_FP\
		  	(\
		  		arg_file,\
		  		arg_format_str,\
		  		##__VA_ARGS__\
		  	);\
		  }\
		  \
		  else\
		  {\
		  	SLY_NO_OP\
		  	(\
		  		arg_file,\
		  		arg_format_str,\
		  		##__VA_ARGS__\
		  	);\
		  }\
		\
		} while (0)




	#if (SLY_NULL_CHECK_ENABLE)


		/// Prints out a debugging message indicating that a pointer is
		/// NULL.
		///
		/// @param pointer		A pointer that may or may not
		///				be NULL

		#define SLY_NULL_CHECK(pointer)\
			\
			do {\
			\
			  SLY_DEBUG_CANARY("SLY_NULL_CHECK");\
			  \
			  \
			  const void * arg_pointer = (pointer);\
			  \
			  const char * str_pointer = (#pointer);\
			  \
			  \
			  if (NULL == arg_pointer)\
			  {\
				SLY_PRINT_DEBUG\
				(\
					"\"%s\" "\
					"evaluated to NULL",\
					str_pointer\
				);\
			  }\
			\
			} while (0)


		/// Version of SLY_NULL_CHECK () that accepts an additional
		/// (FILE *) argument.
		///
		/// Prints out a debugging message indicating that a pointer is
		/// NULL.
		///
		/// @param file			A (FILE *) for an open file,
		///				which the debug message will
		///				be printed to
		///
		/// @param pointer		A pointer that may or may not
		///				be NULL

		#define SLY_NULL_CHECK_FP(file, pointer) \
			\
			do {\
			\
			  SLY_DEBUG_CANARY("SLY_NULL_CHECK_FP");\
			  \
			  \
			  FILE * arg_file = (file);\
			  \
			  const void * arg_pointer = (pointer);\
			  \
			  const char * str_pointer = (#pointer);\
			  \
			  \
			  if (NULL == arg_pointer)\
			  {\
				SLY_PRINT_DEBUG_FP\
				(\
					arg_file,\
					"\"%s\" "\
					"evaluated to NULL",\
					str_pointer\
				);\
			  }\
			\
			} while (0)


	#else //SLY_NULL_CHECK_ENABLE


		#define SLY_NULL_CHECK(pointer)	\
			\
			do {\
			\
			  SLY_DEBUG_CANARY("SLY_NULL_CHECK");\
			  \
			  SLY_NO_OP (pointer);\
			\
			} while (0)


		#define SLY_NULL_CHECK_FP(file, pointer) \
			\
			do {\
			\
			  SLY_DEBUG_CANARY("SLY_NULL_CHECK_FP");\
			  \
			  SLY_NO_OP ((file), (pointer));\
			\
			} while (0)


	#endif //SLY_NULL_CHECK_ENABLE


  #else	//SLY_DEBUG_ENABLE


	//These all get defined as nothing when #SLY_DEBUG_ENABLE is 0, but
	//remember, we still have to preserve argument side-effects!


	#define SLY_DUP_DEBUG_FILE_PTR(file_ptr) \
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_DUP_DEBUG_FILE_PTR");\
		  \
		  SLY_NO_OP \
		  (\
		  	(1),\
		  	(SLY_COMPLAIN_DEFAULT),\
		  	(file_ptr),\
		  	__FILE__,\
		   	__func__,\
		  	__LINE__\
		  );\
		\
		} while (0)


	#define SLY_SET_DEBUG_FILE(truncate_flag,file_path) \
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_SET_DEBUG_FILE");\
		  \
		  SLY_NO_OP \
		  (\
		  	(1),\
		  	(SLY_COMPLAIN_DEFAULT),\
		  	(truncate_flag),\
		  	(file_path),\
		  	__FILE__,\
		   	__func__,\
		  	__LINE__\
		  );\
		\
		} while (0)


	#define SLY_FLUSH_DEBUG_FILE() \
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_FLUSH_DEBUG_FILE");\
		  \
		  SLY_NO_OP \
		  (\
		  	(1),\
		  	(SLY_COMPLAIN_DEFAULT),\
		  	__FILE__,\
		   	__func__,\
		  	__LINE__\
		  );\
		\
		} while (0)


	#define SLY_CLOSE_DEBUG_FILE() \
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_CLOSE_DEBUG_FILE");\
		  \
		  SLY_NO_OP \
		  (\
		  	(1),\
		  	(SLY_COMPLAIN_DEFAULT),\
		  	__FILE__,\
		   	__func__,\
		  	__LINE__\
		  );\
		\
		} while (0)


	#define SLY_PRINT_DEBUG(format_str, ...) \
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_PRINT_DEBUG");\
		  \
		  SLY_NO_OP\
		  (\
		   	(1),\
		  	\
		  	(SLY_MAX_STR_LEN_DEFAULT),\
		  	(SLY_PRINT_TRUNCATE_POST_TEXT),\
		  	\
		  	(SLY_PRINT_FLUSH_DEFAULT),\
		  	\
		  	(SLY_COMPLAIN_DEFAULT),\
		  	\
		  	(SLY_PRINT_BT_NUMBERED_DEFAULT),\
		  	\
		  	(SLY_PRINT_BT_MAX_DEPTH_DEFAULT),\
		  	(SLY_PRINT_BT_TRIM_LOW_DEFAULT),\
		  	(SLY_PRINT_BT_TRIM_HIGH_DEFAULT),\
		  	\
		  	(SLY_PRINT_SOURCE_FILE_PRE_TEXT),\
		  	__FILE__,\
		  	(SLY_PRINT_SOURCE_FILE_POST_TEXT),\
		  	(SLY_PRINT_FUNCTION_PRE_TEXT),\
		  	__func__,\
		  	(SLY_PRINT_FUNCTION_POST_TEXT),\
		  	(SLY_PRINT_LINE_PRE_TEXT),\
		  	__LINE__,\
		  	(SLY_PRINT_LINE_POST_TEXT),\
		  	(SLY_PRINT_TIME_PRE_TEXT),\
		  	(SLY_PRINT_TIME_POST_TEXT),\
		  	(SLY_PRINT_BT_PRE_TEXT),\
		  	(SLY_PRINT_BT_POST_TEXT),\
		  	(SLY_PRINT_BT_ENTRY_PRE_TEXT),\
		  	(SLY_PRINT_BT_ENTRY_POST_TEXT),\
		  	(SLY_PRINT_BT_NUMBER_PRE_TEXT),\
		  	(SLY_PRINT_BT_NUMBER_POST_TEXT),\
		  	(SLY_PRINT_OUTER_MESSAGE_PRE_TEXT),\
		  	(SLY_PRINT_OUTER_MESSAGE_POST_TEXT),\
		  	(SLY_PRINT_INNER_MESSAGE_PRE_TEXT),\
		  	(SLY_PRINT_INNER_MESSAGE_POST_TEXT),\
		  	\
		  	(SLY_PRINT_FIELD_STR),\
		  	\
		  	(format_str),\
		  	##__VA_ARGS__\
		  );\
		\
		} while (0)


	#define SLY_PRINT_DEBUG_OPT(	field_str,\
					format_str,\
					...)\
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_PRINT_DEBUG_OPT");\
		  \
		  SLY_NO_OP\
		  (\
		   	(1),\
		  	\
		  	(SLY_MAX_STR_LEN_DEFAULT),\
		  	(SLY_PRINT_TRUNCATE_POST_TEXT),\
		  	\
		  	(SLY_PRINT_FLUSH_DEFAULT),\
		  	\
		  	(SLY_COMPLAIN_DEFAULT),\
		  	\
		  	(SLY_PRINT_BT_NUMBERED_DEFAULT),\
		  	\
		  	(SLY_PRINT_BT_MAX_DEPTH_DEFAULT),\
		  	(SLY_PRINT_BT_TRIM_LOW_DEFAULT),\
		  	(SLY_PRINT_BT_TRIM_HIGH_DEFAULT),\
		  	\
		  	(SLY_PRINT_SOURCE_FILE_PRE_TEXT),\
		  	__FILE__,\
		  	(SLY_PRINT_SOURCE_FILE_POST_TEXT),\
		  	(SLY_PRINT_FUNCTION_PRE_TEXT),\
		  	__func__,\
		  	(SLY_PRINT_FUNCTION_POST_TEXT),\
		  	(SLY_PRINT_LINE_PRE_TEXT),\
		  	__LINE__,\
		  	(SLY_PRINT_LINE_POST_TEXT),\
		  	(SLY_PRINT_TIME_PRE_TEXT),\
		  	(SLY_PRINT_TIME_POST_TEXT),\
		  	(SLY_PRINT_BT_PRE_TEXT),\
		  	(SLY_PRINT_BT_POST_TEXT),\
		  	(SLY_PRINT_BT_ENTRY_PRE_TEXT),\
		  	(SLY_PRINT_BT_ENTRY_POST_TEXT),\
		  	(SLY_PRINT_BT_NUMBER_PRE_TEXT),\
		  	(SLY_PRINT_BT_NUMBER_POST_TEXT),\
		  	(SLY_PRINT_OUTER_MESSAGE_PRE_TEXT),\
		  	(SLY_PRINT_OUTER_MESSAGE_POST_TEXT),\
		  	(SLY_PRINT_INNER_MESSAGE_PRE_TEXT),\
		  	(SLY_PRINT_INNER_MESSAGE_POST_TEXT),\
		  	\
		  	(field_str),\
		  	\
		  	(format_str),\
		  	##__VA_ARGS__\
		  );\
		\
		} while (0)


	#define SLY_PRINT_DEBUG_ALL(	max_str_len,\
					truncation_post_str,\
					\
					flush_flag,\
					\
					complain_flag,\
					\
					backtrace_numbered_flag,\
					\
					backtrace_max_depth,\
					backtrace_trim_depth_low,\
					backtrace_trim_depth_high,\
					\
					source_file_pre_str,\
					source_file_str,\
					source_file_post_str,\
					function_name_pre_str,\
					function_name_str,\
					function_name_post_str,\
					line_number_pre_str,\
					line_number,\
					line_number_post_str,\
					time_pre_str,\
					time_post_str,\
					backtrace_pre_str,\
					backtrace_post_str,\
					backtrace_entry_pre_str,\
					backtrace_entry_post_str,\
					backtrace_num_pre_str,\
					backtrace_num_post_str,\
					outer_message_pre_str,\
					outer_message_post_str,\
					inner_message_pre_str,\
					inner_message_post_str,\
					\
					field_str,\
					\
					format_str,\
					...)\
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_PRINT_DEBUG_ALL");\
		  \
		  SLY_NO_OP\
		  (\
		  	(1),\
		  	\
		  	(max_str_len),\
		  	(truncation_post_str),\
		  	\
		  	(flush_flag),\
		  	\
		  	(complain_flag),\
		  	\
		  	(backtrace_numbered_flag),\
		  	\
		  	(backtrace_max_depth),\
		  	(backtrace_trim_depth_low),\
		  	(backtrace_trim_depth_high),\
		  	\
		  	(source_file_pre_str),\
		  	(source_file_str),\
		  	(source_file_post_str),\
		  	(function_name_pre_str),\
		  	(function_name_str),\
		  	(function_name_post_str),\
		  	(line_number_pre_str),\
		  	(line_number),\
		  	(line_number_post_str),\
		  	(time_pre_str),\
		  	(time_post_str),\
		  	(backtrace_pre_str),\
		  	(backtrace_post_str),\
		  	(backtrace_entry_pre_str),\
		  	(backtrace_entry_post_str),\
		  	(backtrace_num_pre_str),\
		  	(backtrace_num_post_str),\
		  	(outer_message_pre_str),\
		  	(outer_message_post_str),\
		  	(inner_message_pre_str),\
		  	(inner_message_post_str),\
		  	\
		  	(field_str),\
		  	\
		  	(format_str),\
		  	##__VA_ARGS__\
		  );\
		\
		} while (0)\


	#define SLY_PRINT_DEBUG_FP(file_ptr, format_str, ...) \
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_PRINT_DEBUG_FP");\
		  \
		  SLY_NO_OP\
		  (\
		   	(file_ptr),\
			\
		   	(1),\
		  	\
		  	(SLY_MAX_STR_LEN_DEFAULT),\
		  	(SLY_PRINT_TRUNCATE_POST_TEXT),\
		  	\
		  	(SLY_PRINT_FLUSH_DEFAULT),\
		  	\
		  	(SLY_COMPLAIN_DEFAULT),\
		  	\
		  	(SLY_PRINT_BT_NUMBERED_DEFAULT),\
		  	\
		  	(SLY_PRINT_BT_MAX_DEPTH_DEFAULT),\
		  	(SLY_PRINT_BT_TRIM_LOW_DEFAULT),\
		  	(SLY_PRINT_BT_TRIM_HIGH_DEFAULT),\
		  	\
		  	(SLY_PRINT_SOURCE_FILE_PRE_TEXT),\
		  	__FILE__,\
		  	(SLY_PRINT_SOURCE_FILE_POST_TEXT),\
		  	(SLY_PRINT_FUNCTION_PRE_TEXT),\
		  	__func__,\
		  	(SLY_PRINT_FUNCTION_POST_TEXT),\
		  	(SLY_PRINT_LINE_PRE_TEXT),\
		  	__LINE__,\
		  	(SLY_PRINT_LINE_POST_TEXT),\
		  	(SLY_PRINT_TIME_PRE_TEXT),\
		  	(SLY_PRINT_TIME_POST_TEXT),\
		  	(SLY_PRINT_BT_PRE_TEXT),\
		  	(SLY_PRINT_BT_POST_TEXT),\
		  	(SLY_PRINT_BT_ENTRY_PRE_TEXT),\
		  	(SLY_PRINT_BT_ENTRY_POST_TEXT),\
		  	(SLY_PRINT_BT_NUMBER_PRE_TEXT),\
		  	(SLY_PRINT_BT_NUMBER_POST_TEXT),\
		  	(SLY_PRINT_OUTER_MESSAGE_PRE_TEXT),\
		  	(SLY_PRINT_OUTER_MESSAGE_POST_TEXT),\
		  	(SLY_PRINT_INNER_MESSAGE_PRE_TEXT),\
		  	(SLY_PRINT_INNER_MESSAGE_POST_TEXT),\
		  	\
		  	(SLY_PRINT_FIELD_STR),\
		  	\
		  	(format_str),\
			##__VA_ARGS__\
		  );\
		\
		} while (0)


	#define SLY_PRINT_DEBUG_FP_OPT(	file_ptr,\
					field_str,\
					format_str,\
					...)\
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_PRINT_DEBUG_FP_OPT");\
		  \
		  SLY_NO_OP\
		  (\
		   	(file_ptr),\
			\
		   	(1),\
		  	\
		  	(SLY_MAX_STR_LEN_DEFAULT),\
		  	(SLY_PRINT_TRUNCATE_POST_TEXT),\
		  	\
		  	(SLY_PRINT_FLUSH_DEFAULT),\
		  	\
		  	(SLY_COMPLAIN_DEFAULT),\
		  	\
		  	(SLY_PRINT_BT_NUMBERED_DEFAULT),\
		  	\
		  	(SLY_PRINT_BT_MAX_DEPTH_DEFAULT),\
		  	(SLY_PRINT_BT_TRIM_LOW_DEFAULT),\
		  	(SLY_PRINT_BT_TRIM_HIGH_DEFAULT),\
		  	\
		  	(SLY_PRINT_SOURCE_FILE_PRE_TEXT),\
		  	__FILE__,\
		  	(SLY_PRINT_SOURCE_FILE_POST_TEXT),\
		  	(SLY_PRINT_FUNCTION_PRE_TEXT),\
		  	__func__,\
		  	(SLY_PRINT_FUNCTION_POST_TEXT),\
		  	(SLY_PRINT_LINE_PRE_TEXT),\
		  	__LINE__,\
		  	(SLY_PRINT_LINE_POST_TEXT),\
		  	(SLY_PRINT_TIME_PRE_TEXT),\
		  	(SLY_PRINT_TIME_POST_TEXT),\
		  	(SLY_PRINT_BT_PRE_TEXT),\
		  	(SLY_PRINT_BT_POST_TEXT),\
		  	(SLY_PRINT_BT_ENTRY_PRE_TEXT),\
		  	(SLY_PRINT_BT_ENTRY_POST_TEXT),\
		  	(SLY_PRINT_BT_NUMBER_PRE_TEXT),\
		  	(SLY_PRINT_BT_NUMBER_POST_TEXT),\
		  	(SLY_PRINT_OUTER_MESSAGE_PRE_TEXT),\
		  	(SLY_PRINT_OUTER_MESSAGE_POST_TEXT),\
		  	(SLY_PRINT_INNER_MESSAGE_PRE_TEXT),\
		  	(SLY_PRINT_INNER_MESSAGE_POST_TEXT),\
		  	\
		  	(field_str),\
		  	\
		  	(format_str),\
		  	##__VA_ARGS__\
		  );\
		\
		} while (0)


	#define SLY_PRINT_DEBUG_FP_ALL(	file_ptr,\
					\
					max_str_len,\
					truncation_post_str,\
					\
					flush_flag,\
					\
					complain_flag,\
					\
					backtrace_numbered_flag,\
					\
					backtrace_max_depth,\
					backtrace_trim_depth_low,\
					backtrace_trim_depth_high,\
					\
					source_file_pre_str,\
					source_file_str,\
					source_file_post_str,\
					function_name_pre_str,\
					function_name_str,\
					function_name_post_str,\
					line_number_pre_str,\
					line_number,\
					line_number_post_str,\
					time_pre_str,\
					time_post_str,\
					backtrace_pre_str,\
					backtrace_post_str,\
					backtrace_entry_pre_str,\
					backtrace_entry_post_str,\
					backtrace_num_pre_str,\
					backtrace_num_post_str,\
					outer_message_pre_str,\
					outer_message_post_str,\
					inner_message_pre_str,\
					inner_message_post_str,\
					\
					field_str,\
					\
					format_str,\
					...)\
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_PRINT_DEBUG_FP_ALL");\
		  \
		  SLY_NO_OP\
		  (\
		   	(file_ptr),\
			\
			(1),\
			\
			(max_str_len),\
			(truncation_post_str),\
			\
			(flush_flag),\
			\
			(complain_flag),\
			\
			(backtrace_numbered_flag),\
			\
			(backtrace_max_depth),\
			(backtrace_trim_depth_low),\
			(backtrace_trim_depth_high),\
			\
			(source_file_pre_str),\
			(source_file_str),\
			(source_file_post_str),\
			(function_name_pre_str),\
			(function_name_str),\
			(function_name_post_str),\
			(line_number_pre_str),\
			(line_number),\
			(line_number_post_str),\
			(time_pre_str),\
			(time_post_str),\
			(backtrace_pre_str),\
			(backtrace_post_str),\
			(backtrace_entry_pre_str),\
			(backtrace_entry_post_str),\
			(backtrace_num_pre_str),\
			(backtrace_num_post_str),\
			(outer_message_pre_str),\
			(outer_message_post_str),\
			(inner_message_pre_str),\
			(inner_message_post_str),\
			\
			(field_str),\
			\
			(format_str),\
			##__VA_ARGS__\
		  );\
		\
		} while (0)


	#define SLY_PRINT_IF(condition, format_str, ...)\
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_PRINT_IF");\
		  \
		  SLY_NO_OP ((condition), (format_str), ##__VA_ARGS__);\
		\
		} while (0)


	#define SLY_PRINT_IF_FP(condition, format_str, ...)\
		\
		do {\
		\
		  SLY_DEBUG_CANARY ("SLY_PRINT_IF_FP");\
		  \
		  SLY_NO_OP ((condition), (format_str), ##__VA_ARGS__);\
		\
		} while (0)


	#define SLY_NULL_CHECK(pointer)	\
		\
		do {\
		\
		  SLY_DEBUG_CANARY("SLY_NULL_CHECK");\
		  \
		  SLY_NO_OP (pointer);\
		\
		} while (0)


	#define SLY_NULL_CHECK_FP(file, pointer) \
		\
		do {\
		\
		  SLY_DEBUG_CANARY("SLY_NULL_CHECK_FP");\
		  \
		  SLY_NO_OP ((file), (pointer));\
		\
		} while (0)


  #endif //SLY_DEBUG_ENABLE


  /// @}








  /// @defgroup		Internal_Sly_Debug_Functions
  ///
  /// These function prototypes are necessary to include in this header so that
  /// the above macros can work. While they are available for a user program to
  /// call directly, doing so would miss the point. If you call one of these
  /// functions directly, then it will always execute in your program
  /// regardless of what debug options you have set. i.e. you effectively can't
  /// turn debugging off.
  ///
  /// @{


  int internal_sly_dup_debug_file_ptr
  (
  	int		use_lock,
  	int		complain_flag,
  	FILE *		debug_file_ptr,

  	const char *	source_file_str,
  	const char *	function_name_str,
  	int		line_number
  );


  int internal_sly_set_debug_file
  (
  	int		use_lock,
  	int		complain_flag,
  	int		truncate_flag,
  	const char *	debug_file_path_str,

  	const char *	source_file_str,
  	const char *	function_name_str,
  	int		line_number
  );


  int internal_sly_flush_debug_file
  (
  	int use_lock,
  	int complain_flag,

  	const char *	source_file_str,
  	const char *	function_name_str,
  	int		line_number
  );


  int internal_sly_close_debug_file
  (
  	int use_lock,
  	int complain_flag,

  	const char *	source_file_str,
  	const char *	function_name_str,
  	int		line_number
  );


  __attribute__ ((format (printf, 35, 36)))

  int internal_sly_print_debug_str
  (
  	char *		dst_str,
  	size_t		dst_str_len,
  	char **		rem_str_ptr,
  	size_t *	rem_str_len_ptr,
  	size_t *	print_len_ptr,
  	size_t *	tally_chars_ptr,
  	int *		str_truncated_ptr,

  	int		complain_flag,

  	int		backtrace_numbered_flag,

  	int		backtrace_max_depth,
  	unsigned int	backtrace_trim_depth_low,
  	unsigned int	backtrace_trim_depth_high,

  	const char *	source_file_pre_str,
  	const char * 	source_file_str,
  	const char * 	source_file_post_str,
  	const char * 	function_name_pre_str,
  	const char * 	function_name_str,
  	const char * 	function_name_post_str,
  	const char * 	line_number_pre_str,
  	const int    	line_number,
  	const char * 	line_number_post_str,
  	const char * 	time_pre_str,
  	const char * 	time_post_str,
  	const char * 	backtrace_pre_str,
  	const char * 	backtrace_post_str,
  	const char *	backtrace_entry_pre_str,
  	const char *	backtrace_entry_post_str,
  	const char *	backtrace_num_pre_str,
  	const char *	backtrace_num_post_str,
  	const char * 	outer_message_pre_str,
  	const char * 	outer_message_post_str,
  	const char * 	inner_message_pre_str,
  	const char * 	inner_message_post_str,

  	const char *	field_str,

  	const char *	format_str,
  	...
  );


  __attribute__ ((format (printf, 33, 34)))

  int internal_sly_print_debug_file_ptr
  (
  	FILE *		dst_file_ptr,

  	int		use_lock,

  	size_t		max_str_len,
  	const char *	truncation_post_str,

  	int		flush_flag,

  	int		complain_flag,

  	int		backtrace_numbered_flag,

  	int		backtrace_max_depth,
  	unsigned int	backtrace_trim_depth_low,
  	unsigned int	backtrace_trim_depth_high,

  	const char *	source_file_pre_str,
  	const char * 	source_file_str,
  	const char * 	source_file_post_str,
  	const char * 	function_name_pre_str,
  	const char * 	function_name_str,
  	const char * 	function_name_post_str,
  	const char * 	line_number_pre_str,
  	const int    	line_number,
  	const char * 	line_number_post_str,
  	const char * 	time_pre_str,
  	const char * 	time_post_str,
  	const char * 	backtrace_pre_str,
  	const char * 	backtrace_post_str,
  	const char *	backtrace_entry_pre_str,
  	const char *	backtrace_entry_post_str,
  	const char *	backtrace_num_pre_str,
  	const char *	backtrace_num_post_str,
  	const char * 	outer_message_pre_str,
  	const char * 	outer_message_post_str,
  	const char * 	inner_message_pre_str,
  	const char * 	inner_message_post_str,

  	const char *	field_str,

  	const char *	format_str,
  	...
  );


  __attribute__ ((format (printf, 32, 33)))

  int internal_sly_print_debug
  (
  	int		use_lock,

  	size_t		max_str_len,
  	const char *	truncation_post_str,

  	int		flush_flag,

  	int		complain_flag,

  	int		backtrace_numbered_flag,

  	int		backtrace_max_depth,
  	unsigned int	backtrace_trim_depth_low,
  	unsigned int	backtrace_trim_depth_high,

  	const char *	source_file_pre_str,
  	const char * 	source_file_str,
  	const char * 	source_file_post_str,
  	const char * 	function_name_pre_str,
  	const char * 	function_name_str,
  	const char * 	function_name_post_str,
  	const char * 	line_number_pre_str,
  	const int    	line_number,
  	const char *	line_number_post_str,
  	const char * 	time_pre_str,
  	const char * 	time_post_str,
  	const char * 	backtrace_pre_str,
  	const char * 	backtrace_post_str,
  	const char *	backtrace_entry_pre_str,
  	const char *	backtrace_entry_post_str,
  	const char *	backtrace_num_pre_str,
  	const char *	backtrace_num_post_str,
  	const char * 	outer_message_pre_str,
  	const char * 	outer_message_post_str,
  	const char * 	inner_message_pre_str,
  	const char * 	inner_message_post_str,

  	const char *	field_str,

  	const char *	format_str,
  	...
  );


  /// This function is special; it takes a variadic number of arguments and
  /// does nothing.
  ///
  /// Why does this exist? This is because in C, arguments to functions can
  /// technically have side-effects (ex: What if you pass the results of
  /// malloc() to a function?). So the proper way to replace a function with
  /// "nothing" is to replace it with a function that does nothing. That way,
  /// the arguments to the no-op function can still have side-effects, even
  /// though the function itself does nothing. Basically, this helps prevent
  /// Heisenbugs when #SLY_DEBUG_ENABLE is turned off.
  ///
  /// Why is this defined here? When #SLY_DEBUG_ENABLE is set to 0, we want
  /// SlyDebug to have no cost on the rest of the program. So, it must be
  /// possible for the compiler to see this function's definition, so it can
  /// completely optimize it out. This function is trivial for an optimizer to
  /// remove, and only the arguments that have side-effects should incur a
  /// run-time cost.

  static inline void internal_sly_no_op (int useless_int, ...)
  {
  	;	//Do nothing
  }


  /// @}




#endif //SLY_DEBUG_H

